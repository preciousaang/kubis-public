<?php

namespace App\Http\Livewire\Inventory;

use Livewire\Component;

class ManageProductDetails extends Component
{
    public $product;

    public function render()
    {
        return view('livewire.inventory.manage-product-details');
    }
}