@section('title', 'Manage Categories')
<div>
    @livewire('nav-bar')
    <div class="container-fluid mt-3">
        <div class="row gy-2 justify-content-center">
            @can('create', App\Models\Category::class)
                <div class="col-md-4">
                    <x-info-card>
                        <x-slot name="title">Create New Category</x-slot>
                        @if (session()->has('category_added'))
                            <div class="alert alert-success">
                                {{ session('category_added') }}
                            </div>
                        @endif
                        <form wire:submit.prevent="addCategory">
                            <div class="mb-3">
                                <label class="form-label" for="categoryName">
                                    Category Name
                                </label>
                                <input type="text" name="name" id="categoryName" wire:model.default="name"
                                    class="form-control @error('name') is-invalid @enderror">
                                <x-invalid-feedback field="name" />
                            </div>
                            <div class="d-grid">
                                <button type="submit" class="btn btn-secondary">Add Category</button>
                            </div>
                        </form>
                    </x-info-card>
                </div>
            @endcan
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header h4  text-center">
                        Manage Categories
                    </div>
                    <div class="card-body">

                        @if (session()->has('category_updated'))
                            <x-alert-success>{{ session('category_updated') }}</x-alert-success>
                        @endif
                        @if (session()->has('category_deleted'))
                            <div class="alert alert-info"> {{ session('category_deleted') }}</div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-center">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        @can('create', App\Models\Category::class)
                                            <th scope="col">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($categories as $category)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $category->name }}</td>
                                            @can('create', App\Models\Category::class)
                                                <td>
                                                    {{-- Edit Modal --}}
                                                    <button wire:click="selectCategory({{ $category }})"
                                                        data-bs-toggle="modal" data-bs-target="#categoryEditModal"
                                                        class="btn btn-sm btn-primary">
                                                        <i class="fa fa-edit"></i>
                                                        Edit
                                                    </button>
                                                    {{-- End Edit Modal --}}
                                                    <button wire:click="selectCategory({{ $category }})"
                                                        data-bs-toggle="modal" data-bs-target="#categoryDeleteModal"
                                                        class="btn btn-sm btn-danger">
                                                        <i class="fa fa-trash"></i>
                                                        Delete
                                                    </button>
                                                </td>
                                            @endcan
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3">
                                                No Categories exist yet
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="d-flex justify-content-end">
                            {{ $categories->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Edit Modal --}}
    <div wire:ignore.self class="modal fade" id="categoryEditModal" data-bs-backdrop="static" tabindex="-1"
        aria-labelledby="categoryEditModal" aria-hidden="true">
        <form wire:submit.prevent="editCategory">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    </div>
                    @if ($selectedCategory)
                        <div class="modal-body">
                            <input type="text" wire:model="selectedCategory.name"
                                class="form-control @error('selectedCategory.name') is-invalid @enderror">
                            <x-invalid-feedback field="selectedCategory.name" />
                        </div>
                        <div class="  modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    @endif
                </div>
            </div>
        </form>
    </div>


    {{-- Delete Modal --}}
    <div wire:ignore.self class="modal fade" id="categoryDeleteModal" data-bs-backdrop="static" tabindex="-1"
        aria-labelledby="categoryDeleteModal" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Category</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                @if ($selectedCategory)
                    <div class="modal-body">
                        <p class="h5">Are you sure want to delete {{ $selectedCategory->name }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" wire:click="deleteCategory({{ $selectedCategory }})"
                            class="btn btn-danger">Delete</button>
                    </div>
                @endif
            </div>
        </div>

    </div>
    <script>
        document.addEventListener('livewire:load', () => {
            const categoryEditModal = new Modal(document.getElementById('categoryEditModal'))
            const categoryDeleteModal = new Modal(document.getElementById('categoryDeleteModal'))
            Livewire.on('category_updated', () => {
                categoryEditModal.hide()
            })
            Livewire.on('category_deleted', () => {
                categoryDeleteModal.hide()
            })
        })
    </script>
</div>
