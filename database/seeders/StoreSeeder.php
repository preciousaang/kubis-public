<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stores')->insert([
            [
                'name' => 'Main Store',
                'is_headquarters' => true,
                'address' => 'The main store address',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Victoria Island store Store',
                'is_headquarters' => false,
                'address' => 'The VI store address',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}