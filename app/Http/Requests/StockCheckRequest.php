<?php

namespace App\Http\Requests;

use App\Models\ProductVariation;
use App\Models\Store;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class StockCheckRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'variation_id' => ['required', function ($attribute, $value, $fail) {
                $variation = ProductVariation::where('id', $value)->whereHas('stocks', function (Builder $query) {
                    $hqStore = Store::where('is_headquarters', true)->first();
                    $query->where('store_id', $hqStore->id);
                })->exists();

                if (!$variation) {
                    $fail('Variation does not exist');
                }
            }],
            'count' => 'required|integer'
        ];
    }
}