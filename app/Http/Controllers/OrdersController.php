<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Models\ProductVariation;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class OrdersController extends Controller
{
    //

    public function index()
    {
        $orders = auth()->user()->orders->orderBy('created_at', 'desc');
        return response()->json(['orders' => $orders]);
    }

    public function single(Request $request, $unique_id)
    {
        $order = Order::with('items')->where('unique_id', $unique_id)->firstOrFail();
        if ($request->user()->cannot('view', $order)) {
            abort(403, 'You do not own this order');
        }
        return response()->json(['order' => new OrderResource($order)]);
    }

    public function create(OrderRequest $request)
    {

        //TODO pay for order
        $order = $request->user()->orders()->create([
            'unique_id' => Str::uuid(),
            'shipping_address' => $request->shipping_address ?? null
        ]);
        foreach ($request->products as $item) {
            $productId = $item['product_variation_id'];
            $price = ProductVariation::find($productId)->hqStock->retail_price;
            $order->items()->create([
                'product_variation_id' => $item['product_variation_id'],
                'quantity' => $item['quantity'],
                'price' => $price,

            ]);
        }
        return response()->json(['order' => new OrderResource($order)]);
    }
}