<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\AuthUserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    //
    public function login(LoginRequest $request)
    {
        $user = User::where('username', $request->username)->orWhere('email', $request->username)->first();
        if (!$user || (!Hash::check($request->password, $user->password) && !Hash::check($request->password, $user->old_password))) {
            return response()->json(['status' => 'error', 'message' => 'Invalid credentials'], 400);
        }

        if (!$user->is_active) {
            return response()->json(['message' => 'Your account is not active'], 403);
        }

        $token = $user->createToken('login')->plainTextToken;

        return response()->json(['user' => new AuthUserResource($user), 'token' => $token]);
    }
}