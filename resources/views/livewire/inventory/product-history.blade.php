@section('title', 'Product History')
<div>
    @livewire('nav-bar')
    <div class="container-fluid">
        <div class="row gy-2">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header text-center">
                        Product History
                    </div>
                    <div class="card-body">
                        <img src="{{ asset('storage/uploads/' . $product->image) }}" alt="" class="img-fluid">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>Name</th>
                                    <td>{{ $product->name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="h5">+/-</th>
                                        <th>Type</th>
                                        <th>Quantity</th>
                                        <th>Reason</th>
                                        <th>Desciption</th>
                                        <th>SKU</th>
                                        <th>Staff</th>
                                        <th>Store</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @forelse ($histories as $history)
                                        <tr>
                                            @if ($history->type === 'add')
                                                <td class="text-success text-center h5 fw-bold">+</td>
                                            @elseif($history->type === 'subtract')
                                                <td class="text-danger text-center h5 fw-bold">-</td>
                                            @else
                                                <td></td>
                                            @endif
                                            @if ($history->type === 'add')
                                                <td class="text-success text-center h5 fw-bold">
                                                    {{ $history->quantity }}
                                                </td>
                                            @elseif($history->type === 'subtract')
                                                <td class="text-danger text-center h5 fw-bold">
                                                    {{ $history->quantity }}
                                                </td>
                                            @else
                                                <td>{{ $history->quantity }}</td>
                                            @endif
                                            <td>{{ $history->type }}</td>
                                            <td>{{ $history->reason }}</td>
                                            <td>{{ $history->description }}</td>
                                            <td>{{ $history->variation->sku }}</td>
                                            <td>{{ $history->user->username }}</td>
                                            <td>{{ $history->store->name }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6">No History</td>
                                        </tr>
                                    @endforelse

                                </tbody>
                            </table>
                        </div>
                        <div class="d-flex justify-content-end">
                            {{ $histories->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
