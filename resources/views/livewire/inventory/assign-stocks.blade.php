@section('title', 'Assign Stocks')
<div>
    @livewire('nav-bar')
    <div class="container-fluid">
        <p class="lead">
            <a href="{{ route('manage-product', $variation->product->slug) }}" class="btn btn-sm btn-outline-info">
                <i class="fa fa-backward"></i> Go back to variation list
            </a>
            &nbsp;&nbsp;All stocks for
            {{ $variation->name }}-{{ $variation->product->name }}-{{ $variation->sku }}
        </p>
        <div class="row gy-2">
            @foreach ($stocks as $stock)
                <div class="col-md-3">
                    @livewire('inventory.assign-stock-to-store', ['stock'=>$stock])
                </div>
            @endforeach
            @if (count($unassignedStores))
                <div class="col-md-3">
                    <x-info-card>
                        <x-slot name="title">
                            <i class="fa fa-plus"></i>&nbsp; Add stock to store
                        </x-slot>
                        <x-alert type="success" message="stock_added" />
                        <form action="" wire:submit.prevent="addStock">
                            <div class="mb-2">
                                <label for="" class="form-label">Store</label>
                                <select name="" id=""
                                    class="form-select @error('newStock.store_id') is-invalid @enderror"
                                    wire:model="newStock . store_id">
                                    <option value="">Select Store</option>
                                    @foreach ($unassignedStores as $store)
                                        <option value="{{ $store->id }}">{{ $store->name }}</option>
                                    @endforeach
                                </select>
                                <x-invalid-feedback field="newStock.store_id" />
                            </div>
                            <div class="mb-2">
                                <label for="" class="form-label">Current Stock</label>
                                <input type="text" name="" id="" wire:model="newStock . current_stock"
                                    class="form-control form-control-sm @error('newStock.current_stock') is-invalid @enderror">
                                <x-invalid-feedback field="newStock.current_stock" />
                            </div>
                            <div class="mb-2">
                                <label for="" class="form-label">Stock Threshold</label>
                                <input type="text" name="" id="" wire:model="newStock.stock_threshold"
                                    class="form-control form-control-sm @error('newStock.stock_threshold') is-invalid @enderror">
                                <x-invalid-feedback field="newStock.stock_threshold" />
                            </div>

                            <div class="mb-2">
                                <label for="" class="form-label">Sellilng Price</label>
                                <input type="text" name="" id="" wire:model="newStock.selling_price"
                                    class="form-control form-control-sm @error('newStock.selling_price') is-invalid @enderror">
                                <x-invalid-feedback field="newStock.selling_price" />
                            </div>
                            <div class="mb-2">
                                <label for="" class="form-label">Cost Per Unit</label>
                                <input type="text" name="" id="" wire:model="newStock.cost_per_unit"
                                    class="form-control form-control-sm @error('newStock.cost_per_unit') is-invalid @enderror">
                                <x-invalid-feedback field="newStock.cost_per_unit" />
                            </div>
                            <div class="mb-2">
                                <div class="d-grid">
                                    <button type="submit" class="btn btn-secondary btn-sm">Assign</button>
                                </div>
                            </div>
                        </form>
                    </x-info-card>
                </div>
            @endif
        </div>
    </div>
    {{-- Nothing in the world is as soft and yielding as water. --}}
</div>
