<?php

namespace App\Http\Livewire\Inventory;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Livewire\Component;
use Livewire\WithFileUploads;


class AddProduct extends Component
{
    use AuthorizesRequests;
    use WithFileUploads;

    public $categories = [];
    public $brands = [];
    public $productImage;
    public $productName;
    public $productCategory;
    public $productBrand;

    public function render()
    {
        return view('livewire.inventory.add-product');
    }

    public function addProduct()
    {
        $this->authorize('create', Product::class);
        $this->validate([
            'productImage' => 'required|image|max:2048',
            'productName' => 'required|string|max:255',
            'productCategory' => 'required|exists:categories,id',
            'productBrand' => 'required|exists:brands,id'
        ]);
        $product = new Product();
        $product->name = $this->productName;
        $product->category_id = $this->productCategory;
        $product->brand_id = $this->productBrand;
        $product->image = basename($this->productImage->store('public/uploads'));
        $product->save();

        session()->flash('product_added', 'Product Added');
        $this->resetExcept(['categories', 'brands']);

        // $this->emitSelf('productAdded');
        $this->emitUp('productAdded');
    }
}