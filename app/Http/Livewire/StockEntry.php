<?php

namespace App\Http\Livewire;

use Livewire\Component;

class StockEntry extends Component
{
    public function render()
    {
        return view('livewire.stock-entry');
    }
}
