@section('title', 'Manage Order')
<div>
    @livewire('nav-bar')
    <div class="container-fluid">
        <div class="card">
            @if ($order !== null)
                <div class="card-header h4 text-center">Order #{{ $order->unique_id }}</div>
                <div class="card-body">
                    <div class="table-responsive h6 text-center">
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <tr>
                                    <th>ID</th>
                                    <th> {{ $order->unique_id }}</th>
                                </tr>
                                <tr>
                                    <th>
                                        Status
                                    </th>
                                    <td>
                                        @if ($order->status === 'pending')
                                            <span
                                                class="badge bg-warning text-dark">{{ ucfirst($order->status) }}</span>
                                        @elseif($order->status === 'completed')
                                            <span class="badge bg-success">{{ ucfirst($order->status) }}</span>
                                        @elseif($order->status === 'processing')
                                            <span class="badge bg-primary">{{ ucfirst($order->status) }}</span>
                                        @else
                                            <span class="badge bg-danger">{{ ucfirst($order->status) }}</span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Items</th>
                                    <td>
                                        <div class="row">
                                            @foreach ($order->items as $item)
                                                <div class="col-6">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <tbody>

                                                                <tr>
                                                                    <td class="fw-bold">
                                                                        Name
                                                                    </td>
                                                                    <td>
                                                                        {{ $item->product->name }}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="fw-bold">
                                                                        Variation
                                                                    </td>
                                                                    <td>
                                                                        {{ $item->variation->name }}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="fw-bold">
                                                                        Quantity
                                                                    </td>
                                                                    <td>
                                                                        {{ $item->quantity }}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="fw-bold">
                                                                        Price
                                                                    </td>
                                                                    <td>
                                                                        &#8358;{{ $item->price }}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="fw-bold">
                                                                        Image
                                                                    </td>
                                                                    <td>
                                                                        @if ($item->variation->image)
                                                                            <img alt="product-image"
                                                                                class="w-25 img-fluid"
                                                                                src="{{ asset('storage/uploads/' . $item->variation->image) }}">
                                                                        @else
                                                                            <img alt="product-image"
                                                                                class="w-25  img-fluid"
                                                                                src="{{ asset('storage/uploads/' . $item->product->image) }}">
                                                                        @endif
                                                                    </td>
                                                                </tr>


                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                            @endforeach
                                        </div>



                                    </td>
                                </tr>
                            </tbody>

                        </table>
                    </div>

                </div>
            @endif
        </div>
    </div>
</div>
