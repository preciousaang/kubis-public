<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //TODO create measurement seeder
        //TODO: create brand seeder
        DB::table('brands')->insert([
            [
                'name' => 'Gucci',
                'contact' => 'gucci@mail.com',
                'address' => 'gucci@address'
            ],
            [
                'name' => 'Versace',
                'contact' => 'versace@mail.com',
                'address' => 'versace@address'
            ]
        ]);
    }
}