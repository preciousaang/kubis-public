@section('title', 'Login')
<div class="vh-100 vw-100 bg-info row justify-content-center align-items-center ">
    <div class="col-md-3 col-sm-6 col-12">
        <div class="card shadow">
            <div class="card-header text-center bg-info text-white fw-bold">
                Login
            </div>
            <div class="card-body">
                <x-alert type="warning" message="login_error" />
                <form wire:submit.prevent="login">
                    <div class="mb-3">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" id="username" wire:model.defer="username"
                            class="form-control @error('username') is-invalid @enderror">
                        @error('username')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" id="password" wire:model.defer="password"
                            class="form-control @error('password') is-invalid @enderror">
                        @error('password')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="d-grid">
                        <button type="submit"
                            class="btn btn-block btn-info text-white d-flex justify-content-center align-items-center gap-2">
                            <div class="spinner-border text-light spinner-border-sm" role="status" wire:loading>
                                <span class="visually-hidden">Loading...</span>
                            </div>
                            <i wire:loading.remove class="fa fa-sign-in"></i>
                            Login
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="d-flex justify-content-center mt-4">
            <a href="{{ route('password.request') }}" class="text-decoration-none text-center text-white">
                Forgot Password?
            </a>
        </div>
    </div>
</div>
