<div class="card">
    <div class="card-header bg-info text-white text-center">
        {{ $title }}
    </div>
    <div class="card-body">
        {{ $slot }}
    </div>
</div>
