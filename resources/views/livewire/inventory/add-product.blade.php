<x-info-card>
    <x-slot name="title">
        Add New Product
    </x-slot>

    @if (count($categories) == 0 || count($brands) == 0)
        <div class="text-warning text-center">
            Please create categories and brands before coming to add products.
        </div>
    @endif


    <x-alert message="product_added" type="success" />
    {{-- form for adding product --}}
    <form wire:submit.prevent="addProduct">
        @if ($productImage)
            <div class="w-25">
                <img class="img-fluid rounded shadow" src="{{ $productImage->temporaryUrl() }}">
            </div>
        @endif
        <div class="mb-2">
            <label for="image" class="form-label">Image</label>
            <input accept="image/*" type="file" name="image"
                class="form-control  @error('productImage') is-invalid @enderror" wire:model="productImage" id="image">
            <x-invalid-feedback field="productImage" />
        </div>
        <div class="mb-2">
            <label for="productName" class="form-label">Name</label>
            <input type="text" class="form-control @error('productName') is-invalid @enderror" wire:model="productName"
                id="productName">
            <x-invalid-feedback field="productName" />

        </div>
        <div class="mb-2">
            <label for="productCategory" class="form-label">Product Category</label>
            <select wire:model="productCategory" id="productCategory"
                class="form-select @error('productCategory') is-invalid @enderror">
                <option value="">Select Category</option>
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>

            <x-invalid-feedback field="productCategory" />

        </div>
        <div class="mb-2">
            <label for="productBrand" class="form-label">Product Brand</label>
            <select wire:model="productBrand" id="productBrand"
                class="form-select @error('productBrand') is-invalid @enderror">
                <option value="">Select Brand</option>
                @foreach ($brands as $brand)
                    <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                @endforeach
            </select>
            <x-invalid-feedback field="productBrand" />
        </div>
        <div class="d-grid">
            <button type="submit" class="btn btn-secondary">Add Product</button>
        </div>
    </form>
</x-info-card>
