@section('title', 'Update Stock')
<div>
    @livewire('nav-bar')
    <div class="row my-2 justify-content-center ">
        <div class="col-6">
            <div class="card">
                <div class="card-header text-center text-white bg-info">
                    Manually Adjust Stock
                </div>
                <div class="card-body">
                    <x-alert type="success" message="stock_edited" />
                    <form wire:submit.prevent="updateStock">
                        <div class="mb-2">
                            <label for="productName" class="form-label">Product Name Variation</label>
                            <input type="text" name="" disabled id=""
                                value="{{ $productName }} - {{ $variationName }}" class="form-control">
                        </div>
                        <div class="mb-2">
                            <label for="option" class="form-label">Option</label>
                            <select wire:model="option" id="option"
                                class="form-select @error('option') is-invalid @enderror">
                                <option value="subtract">Subtract</option>
                                <option value="add">Add</option>
                                <option value="override">Override</option>
                            </select>
                            <x-invalid-feedback field="option" />
                        </div>
                        <div class="mb-2">
                            <label for="quantity" class="form-label">Quantity</label>
                            <input type="text" id="quantity"
                                class="form-control @error('quantity') is-invalid @enderror" wire:model="quantity">
                            <x-invalid-feedback field="quantity" />
                        </div>
                        <div class="mb-2">
                            <label for="reason" class="form-label">Reason</label>
                            <textarea id="reason" rows="8" class="form-control @error('reason') is-invalid @enderror"
                                wire:model="reason"></textarea>
                            <x-invalid-feedback field="reason" />
                        </div>
                        <div class="mb-2 d-grid">
                            <button type="submit" class="btn btn-success">Update Stock</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
