@section('title', 'Dashboard')
<div>
    @livewire('nav-bar')
    @if ($user->is_admin)
        @livewire('admin-dashboard')
    @elseif($user->is_manager)
        @livewire('manager-dashboard', ['store'=>$store])
    @else
        @livewire('staff-dashboard')
    @endif


</div>
