<div>
    <nav class="navbar navbar-expand-lg navbar-light bg-info">
        <div class="container-fluid">
            <a href="{{ route('dashboard') }}" class="navbar-brand text-white fw-bold fs-5">
                {{ config('app.name') }} @if ($user->role && ($user->role->name == 'manager' || $user->role->name == 'staff'))
                    - ({{ $user->store->name }})
                @endif
            </a>
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="text-white text-decoration-none nav-link dropdown-toggle" aria-expanded="false" href="#"
                        data-bs-toggle="dropdown" id="dropdownMenuButton">
                        {{ auth()->user()->full_name }} ({{ auth()->user()->role->full_name }})
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <li><a class="dropdown-item" href="{{ route('password.change') }}">Change Password</a></li>
                        <li><a wire:click.prevent="logout" class="dropdown-item" href="#">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <nav class="navbar navbar-expand-lg navbar-light bg-white">
        <div class="container">
            <ul class="navbar-nav me-auto">
                <li class="nav-item">
                    <a href="{{ route('dashboard') }}" class="nav-link">
                        <i class="fa fa-dashboard"></i> Dashboard
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="inventoryDropdown" aria-expanded="false"
                        role="button" data-bs-toggle="dropdown">
                        <i class="fa fa-tag"></i>
                        Inventory
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="inventoryDropdown">
                        <li>
                            <a class="dropdown-item" href="{{ route('inventory-categories') }}">
                                <i class="fa fa-list"></i> &nbsp; Categories
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="{{ route('inventory-products') }}">
                                <i class="fa fa-shopping-bag"></i> &nbsp; Products
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="{{ route('inventory-product-stocks') }}">
                                <i class="fa fa-list"></i> &nbsp; Stock
                            </a>
                        </li>
                        @can('create', App\Models\Brand::class)
                            <li>
                                <a class="dropdown-item" href="{{ route('inventory-manage-brands') }}">
                                    <i class="fa fa-tag"></i> &nbsp; Brands
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="salesDropdown" aria-expanded="false" role="button"
                        data-bs-toggle="dropdown">
                        <i class="fa fa-tag"></i>
                        Sales
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="salesDropdown">
                        @can('viewAny', App\Models\Order::class)
                            <li>
                                <a class="dropdown-item" href="{{ route('manage-orders') }}">
                                    <i class="fa fa-shopping-cart"></i> &nbsp; Orders
                                </a>
                            </li>
                        @endcan
                        <li>
                            <a href="{{ route('manage-sales') }}" class="dropdown-item">
                                <i class="fa fa-money"></i>&nbsp; Manage Sales
                            </a>
                        </li>
                    </ul>
                </li>

                @if (Illuminate\Support\Facades\Gate::allows('can-configure'))
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" id="inventoryDropdown"
                            aria-expanded="false" role="button" data-bs-toggle="dropdown">
                            <i class="fa fa-cog"></i>
                            Configurations
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="inventoryDropdown">
                            <li>
                                <a class="dropdown-item" href="{{ route('manage-stores') }}">
                                    <i class="fa fa-building"></i> &nbsp; Stores
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="{{ route('configuration-units') }}">
                                    <i class="fa fa-pencil"></i>&nbsp; Measurement Units
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
                @can('viewAny', App\Models\User::class)
                    <li class="nav-item">
                        <a href="{{ route('manage-users') }}" class="nav-link">
                            <i class="fa fa-users"></i> Users
                        </a>
                    </li>
                @endcan
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" id="requestDropdown" aria-expanded="false"
                        data-bs-toggle="dropdown" role="button">
                        <i class="fa fa-mail-forward"></i>&nbsp;
                        Requested Stocks</a>
                    <ul class="dropdown-menu" aria-labelledby="requestDropdown">
                        <li>
                            <a href="{{ route('inventory-requests-from-store') }}" class="dropdown-item">
                                <i class="fa fa-mail-forward"></i>&nbsp;
                                Requests from Store</a>
                            <a href="{{ route('inventory-requests-to-store') }}" class="dropdown-item">
                                <i class="fa fa-mail-reply"></i>&nbsp;
                                Store Requests</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>

</div>
