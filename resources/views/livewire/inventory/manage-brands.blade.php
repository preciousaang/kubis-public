@section('title', 'Manage Brands')
<div>
    @livewire('nav-bar')
    <div class="container-fluid">
        <div class="row gy-2 justify-content-center">
            @can('create', App\Models\Brand::class)
                <div class="col-md-4">
                    @livewire('inventory.add-brand')
                </div>
            @endcan
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">View All Brands</h3>
                        <x-alert type="success" message="brand_edited" />
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-center">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Contact</th>
                                        <th>Address</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($brands as $brand)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $brand->name }} </td>
                                            <td>{{ $brand->contact }}</td>
                                            <td>{{ substr($brand->address, 0, 30) . __('...') }}</td>
                                            <td>
                                                <button wire:click="selectBrand({{ $brand }})"
                                                    data-bs-toggle="modal" data-bs-target="#viewBrandModal"
                                                    class="btn btn-success btn-sm" type="button" role="button">View
                                                    Brand</button> &nbsp;
                                                <button wire:click="selectBrand({{ $brand }})"
                                                    data-bs-toggle="modal" data-bs-target="#editBrandModal"
                                                    class="btn btn-primary btn-sm">Edit Brand</button>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5">You have no brands registered. Please register</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-end">
                                {{ $brands->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-modal id="viewBrandModal">
        <x-slot name="title">View Brand</x-slot>
        @if (isset($selectedBrand))
            <table class="table">
                <tbody>
                    <tr>
                        <th>Name</th>
                        <td>{{ $selectedBrand->name }}</td>
                    </tr>
                    <tr>
                        <th>Contact</th>
                        <td>{{ $selectedBrand->contact }}</td>
                    </tr>
                    <tr>
                        <th>Address</th>
                        <td>{{ $selectedBrand->address }}</td>
                    </tr>
                </tbody>
            </table>
        @else
            <h3 class="card-title text-center">Loading...</h3>
        @endIf
    </x-modal>
    <x-modal id="editBrandModal">
        <x-slot name="title">Edit Brand</x-slot>

        <form wire:submit.prevent="editBrand" action="">
            <div class="mb-3">
                <label for="" class="form-label">
                    Brand Name
                </label>
                <input type="text" class="form-control" wire:model="selectedBrand.name">
            </div>
            <div class="mb-3">
                <label for="" class="form-label">
                    Brand Contact
                </label>
                <input type="text" class="form-control" wire:model="selectedBrand.contact">
            </div>
            <div class="mb-3">
                <label for="" class="form-label">
                    Brand Address
                </label>
                <textarea class="form-control" wire:model="selectedBrand.address"></textarea>
            </div>
            <div class="d-grid">
                <button type="submit" class="btn btn-primary">Edit Brand</button>
            </div>
        </form>
    </x-modal>
    <script>
        document.addEventListener('livewire:load', () => {
            document.addEventListener('DOMContentLoaded', () => {
                const editModalEl = selectId('editBrandModal')
                const editModal = new Modal(editModalEl);
                Livewire.on('brandEdited', () => {
                    editModal.hide()
                })
            })

        })
    </script>
</div>
