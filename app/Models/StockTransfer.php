<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockTransfer extends Model
{
    use HasFactory;
    protected $fillable = [
        'from',
        'to',
        'accepted_by',
        'sent_by',
        'status',
        'stock_request_id',
    ];

    public function items()
    {
        return $this->hasMany(StockTransferItem::class);
    }
}