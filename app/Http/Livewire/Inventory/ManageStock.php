<?php

namespace App\Http\Livewire\Inventory;

use App\Models\ProductHistory;
use App\Models\ProductVariation;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Livewire\Component;


class ManageStock extends Component
{
    public $option = "subtract";
    public $stock = null;
    public $quantity;
    public $reason;
    public $variation;
    public $variationName;
    public $productName;

    protected function getRules()
    {
        return [
            'option' => ['required', Rule::in(['add', 'subtract', 'override'])],
            'quantity' => ['required', 'integer'],
            'reason' => 'required'
        ];
    }

    public function render()
    {
        return view('livewire.inventory.manage-stock');
    }

    public function updateStock()
    {
        $this->validate();
        $history = new ProductHistory();
        // TODO create product history
        switch ($this->option) {
            case 'add':
                $this->stock->current_stock += $this->quantity;
                break;
            case 'subtract':
                if ($this->stock->current_stock < $this->quantity) {
                    $this->addError('quantity', 'Quantity is larger than current available stock');
                }
                $this->stock->current_stock -= $this->quantity;
                break;
            default:
                //Override
                $this->stock->current_stock = $this->quantity;
        }


        $history->type = $this->option;
        $history->user_id = auth()->id();
        $history->store_id = auth()->user()->store_id;
        $history->quantity = $this->quantity;
        $history->reason = $this->reason;
        $history->description = auth()->user()->full_name . ' of user id' . auth()->id() . ' added ' . $this->quantity . 'to stock';
        $this->stock->save();
        session()->flash('stock_edited', 'Stock data edited');
        $history->save();
        $this->resetExcept('variation', 'variationName', 'productName');
    }

    public function mount(Request $request)
    {
        $this->variation = ProductVariation::with(['product'])
            ->findOrFail(request()->get('variation'));
        $this->variationName = $this->variation->name;
        $this->productName = $this->variation->product->name;
        //get stock info
        $this->stock = $this->variation
            ->stocks()
            ->where('store_id', $request->user()->store_id)
            ->firstOrFail();
    }
}