<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductHistory extends Model
{
    const  ADD_TYPE = 'add';
    const SUBTRACT_TYPE = 'subtract';
    const OVERRIDE_TYPE = 'override';
    use HasFactory;
    protected $fillable = [
        'store_id',
        'variation_id',
        'user_id',
        'quantity',
        'type',
        'reason',
        'description'
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function variation()
    {
        return $this->belongsTo(ProductVariation::class, 'product_variation_id');
    }

    public function getProductAttribute()
    {
        return $this->variation->product ?? null;
    }
}