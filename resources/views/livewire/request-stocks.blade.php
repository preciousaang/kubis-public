@section('title', 'Request Product Stocks')
<div>
    @livewire('nav-bar')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header text-center">
                        Request Product Stocks for {{ $product->name }}
                    </div>
                    <div class="card-body">
                        <img src="{{ asset('storage/uploads/' . $product->image) }}" alt="" class="img-fluid">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>Name</th>
                                    <td>{{ $product->name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <x-alert type="success" message="request_sent" />
                        <div class="d-flex my-2 justify-content-end">
                            <button wire:click="requestProduct" type="button" role="button"
                                class="btn  btn-primary">Request Stock</button>
                        </div>
                        <div class="form-control mb-5 @error('requestedStocks') is-invalid @enderror"
                            x-data="{ requestedStocks: @js($requestedStocks) }">
                            @forelse ($requestedStocks->all() as $stock)
                                <span class="badge rounded-pill bg-info text-dark">
                                    {{ $stock['variation']['name'] }} - ({{ $stock['quantity'] }})

                                    <i wire:click="removeStock({{ $stock['variation']['id'] }})"
                                        style="cursor: pointer; margin: 5px" class="fa fa-times"></i>
                                </span>
                            @empty
                                Selected stocks for request
                            @endforelse
                        </div>
                        <x-invalid-feedback field="requestedStocks" />
                        <form>
                            <div class="mb-2">
                                <label for="from" class="form-label">From</label>
                                <select wire:model="store" name="" id=""
                                    class="form-select @error('store') is-invalid @endif">
                                    <option value="">Select store to request product from</option>
                                    @foreach ($stores as $store)
                                        <option value="{{ $store->id }}">
                                            {{ $store->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <x-invalid-feedback field="store" />
                            </div>
                            @foreach ($product->variations as $variation)
                                @livewire('request-stock-item', ['variation'=>$variation], key($variation->id))
                            @endforeach
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
