<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;

    const LOCAL_PURCHASE_TYPE = 'local';
    const ONLINE_PURCHASE_TYPE = 'online';

    protected $fillable = [
        'store_id',
        'unique_id',
        'user_id',
        'purchase_type',
        'payment_type',
        'profit',
        'loss'
    ];

    public function items()
    {
        return $this->hasMany(SaleItem::class, 'sale_id');
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function seller()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}