<?php

namespace Kubis\Repository\SaleItem;


use App\Models\SaleItem;
use Kubis\Repository\BaseRepository;

class SaleItemRepository extends BaseRepository implements SaleItemRepositoryInterface
{



    public function __construct(SaleItem $model)
    {
        parent::__construct($model);
    }




    public function fetchSaleItems($per_page, $opt = [])
    {
        $results = $this->model;

        $results = $results->paginate($per_page);

        return $results;
    }
}