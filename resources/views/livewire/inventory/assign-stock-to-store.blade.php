<x-info-card>
    <x-slot name="title">
        For {{ $stock->store->name }}
    </x-slot>
    <x-alert type="success" message="stock_updated" />
    <form action="" wire:submit.prevent="updateStock">
        <div class="mb-2">
            <label for="" class="form-label">Current Stock</label>
            <input type="text" name="" id="" wire:model="stock.current_stock"
                class="form-control form-control-sm @error('stock.current_stock') is-invalid @enderror">
            <x-invalid-feedback field="stock.current_stock" />
        </div>
        <div class="mb-2">
            <label for="" class="form-label">Stock Threshold</label>
            <input type="text" name="" id="" wire:model="stock.stock_threshold"
                class="form-control form-control-sm @error('stock.stock_threshold') is-invalid @enderror">
            <x-invalid-feedback field="stock.stock_threshold" />
        </div>

        <div class="mb-2">
            <label for="" class="form-label">Selling Price</label>
            <input type="text" name="" id="" wire:model="stock.selling_price"
                class="form-control form-control-sm @error('stock.selling_price') is-invalid @enderror">
            <x-invalid-feedback field="stock.selling_price" />
        </div>
        <div class="mb-2">
            <label for="" class="form-label">Cost Per Unit</label>
            <input type="text" name="" id="" wire:model="stock.cost_per_unit"
                class="form-control form-control-sm @error('stock.cost_per_unit') is-invalid @enderror">
            <x-invalid-feedback field="stock.cost_per_unit" />
        </div>
        <div class="mb-2">
            <div class="d-grid">
                <button type="submit" class="btn btn-info btn-sm">Update Stock</button>
            </div>
        </div>
    </form>
</x-info-card>
