<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'superadmin',
                'full_name' => 'Super Administrator'
            ],
            [
                'name' => 'admin',
                'full_name' => 'Administrator'
            ],
            [
                'name' => 'manager',
                'full_name' => 'Store Manager',
            ],
            [
                'name' => 'staff',
                'full_name' => 'Store Staff'
            ],
            [
                'name' => 'user',
                'full_name' => 'User'
            ]
        ]);
    }
}