<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\VerificationRequest;
use App\Http\Resources\AuthUserResource;
use App\Notifications\VerifyEmail;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function user(Request $request)
    {
        return response()->json(['user' => new AuthUserResource($request->user())]);
    }

    public function verifyEmail(VerificationRequest $request)
    {
        $request->user()->verify();
        return response()->json(['message' => 'Your email has been verified']);
    }

    public function resendVerificationEmail(Request $request)
    {
        $user = $request->user();
        if (!$user->hasVerifiedEmail()) {
            $user->generateVerificationToken();
            $user->notify(new VerifyEmail($user->verification_token));
            return response()->json(['message' => 'You verification token has been sent to your email']);
        }
        return response()->json(['message' => 'You are already verified'], 403);
    }
}