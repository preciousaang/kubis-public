<?php

namespace App\Http\Requests;

use App\Models\ProductVariation;
use App\Models\Store;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            //
            'shipping_request' => 'nullable|string',
            'products' => ['required', 'array'],
            'products.*.product_variation_id' => function ($attribute, $value, $fail) {
                $variation = ProductVariation::find($value);

                if (!$variation || !$variation->has('hqStock') || $variation->hqStock->current_stock <= 0) {
                    $fail("Invalid product");
                }
            },
            'products.*.quantity' => ['required', 'integer']
        ];
    }
}