<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockRequest extends Model
{
    use HasFactory;
    const PENDING_STATUS = 'pending';
    const DECLINED_STATUS = 'declined';
    const APPROVED_STATUS = 'approved';

    protected $fillable = [
        'requesting_store',
        'answering_store',
        'requested_by',
        'approved_by',
        'status',
        'approved_at'
    ];


    public function requested_items()
    {
        return $this->hasMany(StockRequestItem::class, 'stock_request_id');
    }

    public function requester()
    {
        return $this->belongsTo(Store::class, 'requesting_store');
    }

    public function requestee()
    {
        return $this->belongsTo(Store::class, 'answering_store');
    }

    public function requesting_user()
    {
        return $this->belongsTo(User::class, 'requested_by');
    }

    public function approving_user()
    {
        return $this->belongsTo(User::class, 'approved_by');
    }
}