<div class="row gy-2 my-2">
    <div class="col">
        <input class="form-check" wire:model="selected" type="checkbox" name="" id="">
    </div>
    <div class="col">{{ $variation->name }}</div>
    <div class="col">
        <input @if (!$selected) disabled @endif type="number" wire:model="quantity"
            class="form-control form-control-sm @error('quantity') is-invalid @enderror" name="" id=""
            placeholder="Enter Quantity">
        <x-invalid-feedback field="quantity" />
    </div>
    <div class="col">
        <button @if (!$selected) disabled @endif type="button" wire:click="add"
            class="btn btn-sm btn-primary">
            Add
        </button>
    </div>
</div>
