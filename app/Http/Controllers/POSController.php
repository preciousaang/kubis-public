<?php

namespace App\Http\Controllers;

use App\Http\Requests\POSRequest;
use App\Http\Resources\SaleResource;

use Exception;

use Kubis\Services\Sales\SalesServiceInterface;

class POSController extends Controller
{
    //
    private $saleService;

    public function __construct(SalesServiceInterface $saleService)
    {
        $this->saleService = $saleService;
    }
    public function create(POSRequest $request)
    {

        // Step 1: Make the sale
        // Step 2: subtract the stock from the store stock

        try {
            $sale = $this
                ->saleService
                ->doSale($request->products, $request->payment_type, 'local', $request->user());
            return response()->json(['status' => 'successful', 'sale' => new SaleResource($sale)]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}