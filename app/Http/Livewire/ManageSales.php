<?php

namespace App\Http\Livewire;

use App\Exports\SaleItemExport;
use App\Models\Sale;
use App\Models\SaleItem;
use Illuminate\Support\Collection;
use Livewire\Component;
use Livewire\WithPagination;

class ManageSales extends Component
{

    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $per_page = 20;
    public $ds;
    public $store_id;
    public $from;
    public $to;


    protected $rules = [
        'from' => 'required|date',
        'to' => 'required|date'
    ];


    public function fetchSales()
    {
        $salesQuery = SaleItem::whereExists(function ($q) {
            $q->from('sales')
                ->whereStoreId($this->store_id)
                ->whereColumn('sales.id', 'sale_items.sale_id');
        });
        if ($this->from || $this->to) {
            $sales = $salesQuery
                ->whereBetween('created_at', [date($this->from), date($this->to)])
                ->paginate($this->per_page);
        } else {
            $sales = $salesQuery->paginate($this->per_page);
        }

        return $sales;
    }

    public function filterByDate()
    {

        $this->resetPage();
    }




    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }



    public function mount()
    {
        $this->store_id = auth()->user()->store_id;
    }

    public function render()
    {
        $sales = $this->fetchSales();
        return view('livewire.manage-sales', [
            'sales' => $sales
        ]);
    }

    public function exportSales()
    {

        $exportable = new SaleItemExport();
        $exportable->forStore($this->store_id);
        $exportable->forYears($this->from, $this->to);
        return \Excel::download($exportable, 'sales.xlsx');
    }
}