@section('title', 'View Request')
<div x-data>
    @livewire('nav-bar')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-center fw-bold h5">
                        Request Details
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <b>Date of request</b>: {{ $request->created_at->toDateTimeString() }}
                            </div>
                            <div class="col-md-6">
                                <b>Request sent by</b>: {{ $request->requesting_user->full_name }}
                            </div>
                            <div class="col-md-6 d-flex gap-2">
                                <b>Status:</b>
                                @if ($request->status === 'approved')
                                    <p class="text-success fw-bold">{{ $request->status }}</p>
                                @elseif($request->status === 'declined')
                                    <p class="text-danger fw-bold">{{ $request->status }}</p>
                                @else
                                    <p class="text-secondary fw-bold">{{ $request->status }}</p>
                                @endif
                            </div>
                        </div>
                        @foreach ($messages->all() as $message)
                            <p class="text-warning text-center">{{ $message }}</p>
                        @endforeach
                        <hr />
                        <h5 class="text-center fw-bold">Items</h5>
                        <table class="table text-center">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Variation</th>
                                    <th>SKU</th>
                                    <th>Quantity</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($request->requested_items as $item)
                                    <tr>
                                        <td>
                                            {{ $item->product->name }}
                                        </td>
                                        <td>
                                            {{ $item->variation->name }}
                                        </td>
                                        <td>
                                            {{ $item->variation->sku }}
                                        </td>
                                        <td>{{ $item->quantity }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        @if ($request->status === 'pending')
                            <div class="d-flex justify-content-end">
                                <button @if ($messages->count()) disabled @endif wire:click="approve"
                                    type="button" role="button" class="btn btn-success me-2">Approve</button>
                                <button class="btn btn-danger" x-on:click="confirm('Are you sure') && $wire.decline()">
                                    Disapprove
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>
        document.addEventListener('livewire:load', () => {



            @this.on('request_approved', () => {
                alert('Request Approved')
            })
            @this.on('request_declined', () => {
                alert('Request Delined');
            })
        })
    </script>
</div>
