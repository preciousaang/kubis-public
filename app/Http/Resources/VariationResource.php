<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VariationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product_id' => $this->product_id,
            'name' => $this->name,
            'image' => asset('storage/uploads/' . $this->image),
            'slug' => $this->slug,
            'sku' => $this->sku,
            'measurement_value' => $this->measurement_value,
            'measurement_unit' => $this->measurement_unit
        ];
    }
}