<?php

namespace App\Http\Livewire;

use App\Models\Category;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithPagination;

class InventoryCategories extends Component
{
    use AuthorizesRequests;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $name;
    public $selectedCategory;

    public function render()
    {
        return view('livewire.inventory-categories', [
            'categories' => Category::orderBy('name', 'asc')->paginate(20)
        ]);
    }

    protected function getRules()
    {
        //TODO
        return [
            'selectedCategory.name' => [
                'required',
                Rule::unique('categories', 'name')->ignore($this->selectedCategory->id),
            ],

        ];
    }

    public function addCategory()
    {
        $this->authorize('create', Category::class);
        $this->validate([
            'name' => [
                'required',
                'string', 'max:255',
                Rule::unique('categories', 'name')
            ]
        ]);
        $category = new Category();
        $category->name = $this->name;
        $category->save();
        $this->name = null;
        session()->flash('category_added', 'Category added successfully.');
    }


    public function selectCategory(Category $category)
    {
        $this->selectedCategory = $category;
    }

    public function editCategory()
    {
        $this->authorize('create', Category::class);
        $this->validate();
        $this->selectedCategory->save();
        $this->emit('category_updated');
        session()->flash('category_updated', 'Category Updated Succesfully');
    }

    public function deleteCategory()
    {
        $this->authorize('create', Category::class);
        $this->selectedCategory->delete();
        $this->selectedCategory = null;
        session()->flash('category_deleted', 'Category deleted');
        $this->emit('category_deleted');
    }
}