<?php

namespace App\Http\Livewire\Inventory;

use App\Models\Brand;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithPagination;

class ManageBrands extends Component
{
    use AuthorizesRequests;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $selectedBrand;


    protected function getRules()
    {
        return [
            'selectedBrand.name' => ['required', 'string', 'max:255', Rule::unique('brands', 'name')->ignore($this->selectedBrand->id)],
            'selectedBrand.contact' => ['required'],
            'selectedBrand.address' => 'required'
        ];
    }

    public function render()
    {
        $brands = Brand::orderBy('name', 'asc')->paginate(20);
        return view('livewire.inventory.manage-brands', [
            'brands' => $brands
        ]);
    }

    public function selectBrand(Brand $brand)
    {
        $this->selectedBrand = $brand;
    }

    public function editBrand()
    {
        $this->validate();
        $this->selectedBrand->save();
        $this->resetExcept();
        $this->emit('brandEdited');
        session()->flash('brand_edited', 'Brand Edited Successfully');
    }

    public function mount()
    {
        $this->authorize('create', Brand::class);
    }
}