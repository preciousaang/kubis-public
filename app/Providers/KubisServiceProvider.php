<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Kubis\Repository\SaleItem\SaleItemRepository;
use Kubis\Repository\SaleItem\SaleItemRepositoryInterface;
use Kubis\Repository\Sales\SalesRepository;
use Kubis\Repository\Sales\SalesRepositoryInterface;
use Kubis\Services\Sales\SalesService;
use Kubis\Services\Sales\SalesServiceInterface;

class KubisServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(SalesServiceInterface::class, SalesService::class);
        $this->app->bind(SalesRepositoryInterface::class, SalesRepository::class);
        $this->app->bind(SaleItemRepositoryInterface::class, SaleItemRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}