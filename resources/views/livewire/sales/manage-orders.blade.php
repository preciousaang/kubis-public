@section('title', 'Manage Orders')
<div>
    @livewire('nav-bar')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header h4 text-center">
                        Manage Orders
                    </div>
                    <div class="card-body">
                        <div class="d-flex my-2 justify-content-between">
                            <div>
                                <label for="" class="form-label">Filter</label>
                                <select wire:model="status" id="" class="form-select form-select-sm">
                                    <option value="">All</option>
                                    <option value="pending">Pending</option>
                                    <option value="processsing">Processing</option>
                                    <option value="completed">Completed</option>
                                    <option value="cancelled">Cancelled</option>
                                </select>
                                <label for="" class="form-label">Items Per Page</label>
                                <select wire:model="perPage" id="" class="form-select form-select-sm">

                                    @for ($i = 10; $i <= 100; $i += 10)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div>
                                <label for="" class="form-label">Search</label>
                                <input wire:model.debounce.500ms="search" type="text"
                                    class="form-control form-control-sm">
                                <label for="" class="form-label">Filter Date</label>
                                <input wire:model="date" type="date" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table-bordered table text-center">
                                <thead>
                                    <tr>
                                        <th>
                                            Unique ID
                                        </th>
                                        <th>
                                            Customer
                                        </th>
                                        <th>
                                            Items
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                        <th>
                                            Date Created
                                        </th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($orders as $order)
                                        <tr>
                                            <td>{{ $order->unique_id }}</td>
                                            <td>{{ $order->user->full_name }}</td>
                                            <td>{{ $order->items()->count() }}</td>
                                            <td>
                                                @if ($order->status === 'pending')
                                                    <span
                                                        class="badge bg-warning text-dark">{{ ucfirst($order->status) }}</span>
                                                @elseif($order->status === 'completed')
                                                    <span
                                                        class="badge bg-success">{{ ucfirst($order->status) }}</span>
                                                @elseif($order->status === 'processing')
                                                    <span
                                                        class="badge bg-primary">{{ ucfirst($order->status) }}</span>
                                                @else
                                                    <span class="badge bg-danger">{{ ucfirst($order->status) }}</span>
                                                @endif
                                            </td>
                                            <td>{{ $order->created_at }}</td>
                                            <td>

                                                <button wire:click="selectOrder({{ $order }})"
                                                    class="btn btn-sm btn-primary m-2" data-bs-toggle="modal"
                                                    data-bs-target="#viewOrderModal" role="button">View</button>

                                                <a href="{{ route('manage-order', $order) }}"
                                                    class="btn btn-sm btn-primary m-2">Manage
                                                    Order</a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5">No Items</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="d-flex justify-content-end">
                            {{ $orders->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-modal id="viewOrderModal" size="lg">
        @if ($selectedOrder)
            <table class="table table-striped">
                <tr>
                    <th>Uniqe ID</th>
                    <td> {{ $selectedOrder->unique_id }}</td>
                </tr>
                <tr>
                    <th>Customer</th>
                    <td> {{ $selectedOrder->user->full_name }}</td>
                </tr>
                <tr>
                    <th>Items</th>
                    <td> {{ $selectedOrder->items()->count() }}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>
                        @if ($selectedOrder->status === 'pending')
                            <span class="badge bg-warning text-dark">{{ ucfirst($selectedOrder->status) }}</span>
                        @elseif($selectedOrder->status === 'completed')
                            <span class="badge bg-success">{{ ucfirst($selectedOrder->status) }}</span>
                        @elseif($selectedOrder->status === 'processing')
                            <span class="badge bg-primary">{{ ucfirst($selectedOrder->status) }}</span>
                        @else
                            <span class="badge bg-danger">{{ ucfirst($selectedOrder->status) }}</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>Date of Order</th>
                    <td>{{ $selectedOrder->created_at }}</td>
                </tr>

            </table>
        @endif
    </x-modal>
    <script>
        window.addEventListener('livewire:load', () => {

        })
    </script>
</div>
