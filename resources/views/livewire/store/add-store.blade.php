<x-info-card>
    <x-slot name="title">Add Store</x-slot>
    <x-alert type="success" message="store_added" />
    <form wire:submit.prevent="addStore">
        <div class="mb-3">
            <label for="storeName" class="form-label">Store Name</label>
            <input type="text" id="storeName" wire:model="name" class="form-control @error('name') is-invalid @enderror">
            <x-invalid-feedback field="name" />
        </div>
        <div class="mb-3">
            <label for="storeName" class="form-label">Store Address</label>
            <textarea id="storeName" wire:model="address" class="form-control @error('address') is-invalid @enderror"></textarea>
            <x-invalid-feedback field="address" />
        </div>
        <div class="mb-3">
            <div class="d-grid">
                <button type="submit" class="btn btn-secondary">Add Store</button>
            </div>
        </div>
    </form>
</x-info-card>
