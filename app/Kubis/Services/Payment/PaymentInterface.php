<?php

namespace Kubis\Services\Payment;


interface PaymentInterface
{
    public function verifyTransaction(string $transaction_id);
}