<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = DB::table('roles')->where('name', 'admin')->first();
        $managerRole = DB::table('roles')->where('name', 'manager')->first();
        $userRole = DB::table('roles')->where('name', 'user')->first();

        DB::table('users')->insert(
            [
                [
                    'email' => 'preciousaang@gmail.com',
                    'password' => bcrypt('albert'),
                    'first_name' => 'Precious',
                    'last_name' => 'Ibeagi',
                    'phone' => '08039483748',
                    'username' => 'preciousaang',
                    'is_active' => true,
                    'role_id' => $adminRole->id,
                ],

                [
                    'email' => 'preciousone@mail.com',
                    'password' => bcrypt('albert'),
                    'first_name' => 'Precious',
                    'last_name' => 'One',
                    'phone' => '08039283788',
                    'username' => 'preciousone',
                    'is_active' => true,
                    'user_id' => $userRole->id
                ]
            ]
        );
        DB::table('users')->insert([
            [
                'email' => 'precious@mail.com',
                'password' => bcrypt('albert'),
                'first_name' => 'Precious',
                'last_name' => 'Manstonss',
                'phone' => '08039483742',
                'username' => 'precious',
                'store_id' => 2,
                'is_active' => true,
                'role_id' => $managerRole->id,
            ],
            [
                'email' => 'james@mail.com',
                'password' => bcrypt('albert'),
                'first_name' => 'James',
                'last_name' => 'Brown',
                'phone' => '08039283742',
                'username' => 'james',
                'store_id' => 1,
                'is_active' => true,
                'role_id' => $managerRole->id,
            ],
        ]);
    }
}