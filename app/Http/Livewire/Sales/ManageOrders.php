<?php

namespace App\Http\Livewire\Sales;

use App\Models\Order;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class ManageOrders extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search;
    public $selectedOrder;
    public $perPage = 10;
    public $status = "";
    public $date;

    public function render()
    {

        $orders = Order::with('items')
            ->latest()
            ->where(function (Builder $query) {
                $query->where('unique_id', 'like', "%{$this->search}%")
                    ->orWhereRelation('user', 'first_name', 'like', "%{$this->search}%")
                    ->orWhereRelation('user', 'last_name', 'like', "%{$this->search}%");;
            });
        if ($this->status) {
            $orders = $orders->where('status', $this->status);
        }

        if ($this->date) {
            $orders = $orders->whereDate('created_at', $this->date);
        }

        $orders = $orders->paginate($this->perPage);
        return view('livewire.sales.manage-orders', [
            'orders' => $orders
        ]);
    }

    public function updatedPerPage()
    {
        $this->resetPage();
    }

    public function updatedStatus()
    {
        $this->resetPage();
    }

    // TODO view single order
    // create search bar
    // create filter (eg. view by date, view by customer)
    public function selectOrder(Order $order)
    {
        $this->selectedOrder = $order;
    }
}