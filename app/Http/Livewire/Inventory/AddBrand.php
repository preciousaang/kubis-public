<?php

namespace App\Http\Livewire\Inventory;

use App\Models\Brand;
use Livewire\Component;

class AddBrand extends Component
{
    public $name;
    public $contact;
    public $address;
    public function render()
    {
        return view('livewire.inventory.add-brand');
    }
    public function addBrand()
    {
        $this->validate([
            'name' => 'required|unique:brands,name',
            'contact' => 'required|string|max:255',
            'address' => 'required'
        ]);
        $brand = new Brand();
        $brand->name = $this->name;
        $brand->contact = $this->contact;
        $brand->address = $this->address;
        $brand->save();
        session()->flash('brand_added', 'Brand added successfully');
        //Add exempted fields
        $this->resetExcept();
    }
}