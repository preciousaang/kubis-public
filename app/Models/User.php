<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Str;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;


    const ADMIN_ABILITIES = [];
    const MANAGER_ABILITIES = [];
    const USER_ABILITIES = [];




    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'phone',
        'old_password',
        'role_id',
        'is_active',
        'email',
        'store_id',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'old_password',
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

    public function getIsUserAttribute()
    {
        return $this->role && $this->role->name == 'user';
    }


    public function getIsSuperadminAttribute()
    {
        return $this->role && $this->role->name == 'superadmin';
    }


    public function getIsAdminAttribute()
    {
        return $this->role && ($this->role->name === 'superadmin' || $this->role->name === 'admin');
    }

    public function getIsManagerAttribute()
    {
        return $this->role && $this->role->name == 'manager';
    }

    public function getIsHqManagerAttribute()
    {
        return $this->role && $this->role->name === 'manager' && $this->store !== null && $this->store->is_headquarters;
    }

    public function getIsStaffAttribute()
    {
        return $this->role && $this->role->name == 'staff';
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function verify()
    {
        $this->markEmailAsVerified();
        $this->verification_token = null;
        $this->save();
    }

    public function generateVerificationToken()
    {
        $this->verification_token = Str::random(8);
        $this->save();
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}