<?php

namespace Kubis\Services\Payment;

use Illuminate\Support\Facades\Http;

class Paystack implements PaymentInterface
{
    private $apiUrl;
    private $publicKey;
    private $secretKey;


    public function __construct($apiUrl, $publicKey, $secretKey)
    {
        $this->apiUrl = $apiUrl;
        $this->publicKey = $publicKey;
        $this->secretKey = $secretKey;
    }

    private function makeRequest()
    {
        return Http::withToken($this->secretKey)->acceptJson()->baseUrl($this->apiUrl);
    }

    public function verifyTransaction($transaction_id)
    {
        return $this
            ->makeRequest()
            ->get('/transaction/verify/' . $transaction_id)
            ->throw()
            ->json();
    }
}