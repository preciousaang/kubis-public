@section('title', 'Configure Units')
<div>
    <livewire:nav-bar />
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <livewire:configuration.add-unit />
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <h5>
                            View All Measurement Units
                        </h5>
                        @if (session()->has('unit_edited') || session()->has('unit_deleted'))
                            <div class="alert alert-info">
                                {{ session('unit_edited') ?? session('unit_deleted') }}
                            </div>
                        @endif
                        <div class="d-flex justify-content-end my-2">
                            <form>
                                <input type="text" wire:model.debounce.500ms="search"
                                    class="form-control-sm form-control" placeholder="Search Units">
                            </form>
                            <div wire:loading wire:target="search"
                                class="ms-2 spinner-border spinner-border-sm text-info" role="status">
                            </div>
                        </div>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>Unit Name</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($units as $unit)
                                    <tr class="text-center">
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $unit->name }}</td>
                                        <td>
                                            <button wire:click="selectUnit({{ $unit }})"
                                                class="btn btn-sm btn-primary" data-bs-toggle="modal"
                                                data-bs-target="#editUnitModal">
                                                Edit
                                            </button>
                                            &nbsp;
                                            <button wire:click="selectUnit({{ $unit }})"
                                                class="btn btn-sm btn-danger" data-bs-toggle="modal"
                                                data-bs-target="#deleteUnitModal">
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3" class="text-center">
                                            You have't created any measurement unit.
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <div class="d-flex justify-content-end">
                            {{ $units->links() }}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <x-modal id="editUnitModal">
        <x-slot name="title">Edit Unit</x-slot>
        <form wire:submit.prevent="onEdit">
            <div class="mb-3">
                <label for="name" class="form-label">
                    Name
                </label>
                <input type="text" wire:model="selectedUnit.name"
                    class="form-control @error('selected.name') is-invalid @enderror">
                @error('selectedUnit.name')
                    <x-invalid-feedback>{{ $message }}</x-invalid-feedback>
                @enderror
            </div>
            <button class="btn btn-primary" type="submit">Edit Unit</button>
        </form>
    </x-modal>
    <x-modal id="deleteUnitModal">
        <x-slot name="title">Delete Unit</x-slot>
        <p class="lead">Are you Sure</p>
        <button wire:click="onDelete" class="btn btn-sm btn-danger">Yes, Delete.</button>
        <button class="btn btn-sm btn-secondary" data-bs-dismiss="modal">No</button>
    </x-modal>
    <script>
        document.addEventListener('livewire:load', () => {
            const editUnitModal = new Modal(selectId('editUnitModal'))
            const deleteUnitModal = new Modal(selectId('deleteUnitModal'))
            Livewire.on('unitEdited', () => {
                editUnitModal.hide()
            })
            Livewire.on('unitDeleted', () => {
                deleteUnitModal.hide()
            })
        })
    </script>
</div>
