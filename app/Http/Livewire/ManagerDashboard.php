<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ManagerDashboard extends Component
{
    public $store;
    public $lowStocks;
    public $todayRevenue;
    public $todayProfit;


    public function render()
    {
        return view('livewire.manager-dashboard');
    }

    public function fetchLowStocks()
    {
        //compore columns
        $lowStocks = $this->store->stocks()->with(['variation'])->whereColumn('current_stock', '<=', 'stock_threshold')->limit(6)->get();
        $this->lowStocks = $lowStocks;
    }

    public function fetchTodaysSales()
    {
    }

    public function fetchTodayProfit()
    {

        $this->todayProfit = $this
            ->store
            ->soldItems()
            ->whereDate('sale_items.created_at', Carbon::today()->toDateString())
            ->sum('profit');
    }

    public function fetchTodayRevenue()
    {
        $todayRevenue = $this
            ->store
            ->soldItems()
            ->whereDate('sale_items.created_at', Carbon::today()->toDateString())
            ->selectRaw('sum(sale_items.amount * sale_items.quantity) as total_amount')
            ->groupBy('store_id')
            ->first();

        $this->todayRevenue = $todayRevenue['total_amount'] ?? 0;
    }

    public function mount()
    {
        $this->fetchLowStocks();
        $this->fetchTodayRevenue();
        $this->fetchTodayProfit();
    }
}