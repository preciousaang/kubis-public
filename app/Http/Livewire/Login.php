<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Login extends Component
{

    public $username = '', $password = '';
    public function render()
    {
        return view('livewire.login');
    }

    public function getRules()
    {
        return [
            'username' => 'required',
            'password' => 'required'
        ];
    }


    public function login()
    {
        $this->validate();
        $user = User::whereUsername($this->username)->first();
        if ($user == null) {
            $this->addError('username', 'Invalid Credentials');
            return;
        }
        if (!Hash::check($this->password, $user->password) && !Hash::check($this->password, $user->old_password)) {
            //TODO handle case of old password set for user to set their new password
            $this->addError('password', 'Invalid Credentials');
            $this->password = null;
            return;
        }
        if (!$user->is_active) {
            session()->flash('login_error', "You can't log in now. Contact the system administrator");
            return;
        }
        if ($user->is_user) {
            session()->flash('login_error', "You cannot log in here. Please use the website or mobile app.");
            return;
        }

        //check if user is manager or staff, if they are assigned to a store
        if (($user->is_manager || $user->is_staff) && !$user->store) {
            session()->flash('login_error', "You are not assigned to any store. Contact your system administrator");
            return;
        }

        Auth::login($user);
        return redirect()->intended(route('dashboard'));
    }
}