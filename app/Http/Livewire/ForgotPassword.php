<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Password;

use Livewire\Component;

class ForgotPassword extends Component
{

    public $email;

    protected $rules = [
        'email' => 'required|email|exists:users,email'
    ];


    public function sendResetMail()
    {
        $this->validate();

        $status = Password::sendResetLink(
            ['email' => $this->email]
        );

        if ($status === Password::RESET_LINK_SENT) {
            session()->flash('email_sent', 'The reset link has been sent to your email.');
            $this->reset();
        }
    }


    public function render()
    {
        return view('livewire.forgot-password');
    }
}