@section('title', 'Manage Products')
<div>
    <livewire:nav-bar />
    <div class="container-fluid">
        <div class="row gy-2 justify-content-center">
            @can('create', App\Models\Product::class)
                <div class="col-md-4">
                    <livewire:inventory.add-product :brands="$brands" :categories="$categories" />
                </div>
            @endcan
            <div class="col-md-8">
                {{-- Product List --}}

                <div class="card">
                    <div class="card-body">
                        <h4 class="mb-2">View Products</h4>
                        <div class="d-flex justify-content-end align-items-center my-2">
                            {{-- Search Products --}}
                            <form>
                                <input type="text" wire:model.debounce.500ms="search"
                                    class="form-control-sm form-control" placeholder="Search Products">
                            </form>
                            <div wire:loading class="ms-2 spinner-border spinner-border-sm text-info" role="status"
                                wire:target="search">

                            </div>
                        </div>

                        <x-alert type="success" message="product_updated" />

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr class="align-center text-center">
                                        <th>#</th>
                                        <th>Product Name</th>
                                        <th>Image</th>
                                        <th>Category</th>
                                        <th>Brand</th>
                                        <th>Variation Count</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($products as $product)
                                        <tr class="text-center">
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $product->name }}</td>
                                            <td>

                                                <img class="img-fluid rounded" style="height: 100px;"
                                                    src="{{ asset('storage/uploads/' . $product->image) }}"
                                                    alt="{{ $product->name }}">


                                            <td>{{ $product->category->name }}</td>
                                            <td>{{ $product->brand->name }}</td>
                                            <td>{{ $product->variation_count }}</td>
                                            <td>
                                                <div class="dropdown">
                                                    <button class="btn btn-secondary btn-sm dropdown-toggle"
                                                        type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown"
                                                        aria-expanded="false">
                                                        Manage
                                                    </button>
                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                        @can('create', App\Models\Product::class)
                                                            <li>
                                                                <a class="dropdown-item" href="#"
                                                                    wire:click="selectProduct({{ $product }})"
                                                                    data-bs-toggle="modal"
                                                                    data-bs-target="#productEditModal">Edit</a>
                                                            </li>
                                                        @endcan
                                                        <li>
                                                            <a class="dropdown-item"
                                                                href="{{ route('manage-product', ['slug' => $product->slug]) }}">Manage
                                                                Product</a>
                                                        </li>
                                                        <li>
                                                            <a class="dropdown-item"
                                                                href="{{ route('inventory-product-history', $product->id) }}">Product
                                                                History</a>
                                                        </li>
                                                        <li>
                                                            <a class="dropdown-item"
                                                                href="{{ route('inventory-request-stocks', $product) }}">Request
                                                                Stocks</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7" class="text-center">
                                                @if ($search)
                                                    No products by that search criteria .
                                                @else
                                                    You have no products yet
                                                @endif
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="d-flex justify-content-end">
                            {{ $products->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Edit Modal --}}
    <div wire:ignore.self class="modal fade" id="productEditModal" data-bs-backdrop="static" tabindex="-1"
        aria-labelledby="productEditModal" aria-hidden="true">
        <form wire:submit.prevent="editProduct">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Product</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    </div>
                    @if ($selectedProduct)
                        <div class="modal-body">
                            <div class="mb-2">
                                <label for="" class="form-label">Product Name</label>
                                <input type="text" wire:model="selectedProduct.name"
                                    class="form-control @error('selectedProduct.name') is-invalid @enderror">
                                <x-invalid-feedback field="selectedProduct.name" />

                            </div>
                            <div class="mb-2">
                                <label for="" class="form-label">Product Category</label>
                                <select wire:model="selectedProduct.category_id" name="" id=""
                                    class="form-select @error('selectedProduct.category_id') is-invalid @enderror">
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>

                                <x-invalid-feedback field="selectedProduct.category_id" />
                            </div>
                            <div class="mb-2">
                                <label for="" class="form-label">Product Brand</label>
                                <select wire:model="selectedProduct.brand_id" name="" id=""
                                    class="form-select @error('selectedProduct.brand_id') is-invalid @enderror">
                                    @foreach ($brands as $brand)
                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                    @endforeach
                                </select>

                                <x-invalid-feedback field="selectedProduct.brand_id" />
                            </div>
                            @if ($image)
                                <img class="img-fluid rounded" src="{{ $image->temporaryUrl() }}"
                                    style="height: 100px" />
                            @else
                                <img class="img-fluid rounded"
                                    src="{{ asset('storage/uploads/' . $selectedProduct->image) }}"
                                    style="height: 100px" />
                            @endif
                            <div class="mb-2">
                                <label for="" class="form-label">Product Category</label>
                                <input wire:model="image" type="file" accept="image/*"
                                    class="form-control  @error('image') is-invalid @enderror">

                                <x-invalid-feedback field="image" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    @endif
                </div>
            </div>
        </form>
    </div>
    <script>
        document.addEventListener('livewire:load', () => {
            var productEditModal = new Modal(document.getElementById('productEditModal'))
            Livewire.on('productEdited', () => {
                productEditModal.hide()
            });
        })
    </script>
</div>
