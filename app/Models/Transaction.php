<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'reference',
        'amount',
        'order_id',
        'status',
        'paid_at',
        'ip_address',
        'currency',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}