@section('title', 'Change Password')
<div>
    @livewire('nav-bar')
    <div class="container fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header text-center h5">Change Password</div>
                    <div class="card-body">
                        <x-alert type="success" message="password_changed" />
                        <form wire:submit.prevent="changePassword">
                            <div class="form-group mb-2">
                                <label for="old_password" class="form-label">Old Password</label>
                                <input type="password" class="form-control @error('old_password') is-invalid @enderror"
                                    wire:model="old_password">
                                <x-invalid-feedback field="old_password" />
                            </div>
                            <div class="form-group mb-2">
                                <label for="password" class="form-label">New Password</label>
                                <input type="password" class="form-control  @error('password') is-invalid @enderror"
                                    wire:model="password">
                                <x-invalid-feedback field="password" />
                            </div>
                            <div class="form-group mb-2">
                                <label for="password_confirm" class="form-label">Confirm Password</label>
                                <input type="password" class="form-control" wire:model="password_confirmation">
                            </div>
                            <div class="d-grid">
                                <button class="btn btn-info" type="submit">Change Password</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
