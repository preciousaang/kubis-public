<?php

namespace App\Http\Livewire\Store;

use App\Models\Store;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithPagination;

class ManageStore extends Component
{
    use AuthorizesRequests;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $selectedStore;
    public $isHQ;

    protected $listeners = [
        'storeAdded'
    ];


    public function storeAdded()
    {
        $this->resetPage();
    }


    protected function getRules()
    {
        return [
            'selectedStore.name' => ['required', Rule::unique('stores', 'name')->ignore($this->selectedStore->id)],
            'selectedStore.address' => 'required',
            'isHQ' => 'required|boolean'
        ];
    }

    public function mount()
    {
        $this->authorize('viewAny', Store::class);
    }


    public function render()
    {
        $stores = Store::orderBy('is_headquarters', 'desc')->orderBy('name', 'asc')->paginate(10);
        return view('livewire.store.manage-store', ['stores' => $stores]);
    }

    public function selectStore(Store $store)
    {
        $this->selectedStore = $store;
        $this->isHQ = $this->selectedStore->is_headquarters;
    }

    public function editStore()
    {
        //TODO handle case of turning non headquarters to headquarters

        $this->validate();
        if (!$this->selectedStore->is_headquarters && $this->isHQ) {
            //changing current store to the new HQ
            //find current HQ
            $currentHQ = Store::where('is_headquarters', true)->first();
            if ($currentHQ == null) {
                //if an hq doesn't exist, then declare it the hq
                $this->selectedStore->is_headquarters = $this->isHQ;
            } else {
                $currentHQ->is_headquarters = false;
                $currentHQ->save();
                $this->selectedStore->is_headquarters = $this->isHQ;
            }
        } else if ($this->selectedStore->is_headquarters && !$this->isHQ) {
            //changing current store to non-hq
            $this->addError('isHQ', 'A store must be HQ. To set go to the desired store and make it the new HQ');
            return;
        } else {
            $this->selectedStore->is_headquarters = $this->isHQ;
        }

        $this->selectedStore->save();
        $this->emit('storeEdited');
        $this->resetExcept();
        session()->flash('store_edited', 'Store edited successfully');
    }
}