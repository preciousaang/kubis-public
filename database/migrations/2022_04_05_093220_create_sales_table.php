<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->uuid('unique_id');
            $table->foreignId('user_id')->constrained()->comment('The seller ID')->nullable();
            $table->foreignId('store_id')->constrained();
            $table->enum('purchase_type', ['local', 'online']);
            $table->enum('payment_type', ['card', 'cash', 'transfer']);
            //TODO consider the status of the sale
            // $table->enum('status', ['successful', 'failed', 'cancelled', 'processing'])->nullable('processing');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}