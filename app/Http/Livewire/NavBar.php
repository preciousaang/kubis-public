<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Gate;
use Livewire\Component;

class NavBar extends Component
{
    public function render()
    {
        return view('livewire.nav-bar');
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->route('login');
    }

    public function mount()
    {
        $this->user = auth()->user();
    }
}