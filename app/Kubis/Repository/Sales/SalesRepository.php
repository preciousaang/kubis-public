<?php

namespace Kubis\Repository\Sales;

use App\Models\Sale;


class SalesRepository implements SalesRepositoryInterface
{
    private Sale $model;


    public function fetchSales($per_page, $opt = [])
    {
        $results = $this->model->with([]);

        $results = $results->paginate($per_page);

        return $results;
    }
}