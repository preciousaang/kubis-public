<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    public function test_login()
    {
        $response = $this->post('/api/auth/login', [
            'username' => 'preciousaang',
            'password' => 'albert'
        ]);

        $response->assertStatus(200);
    }
}