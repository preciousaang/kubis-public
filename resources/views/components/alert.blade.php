@if (session()->has($message))
    <div class="alert alert-{{ trim($type) }} text-center">
        {{ session($message) }}
    </div>
@endif
