<x-info-card>
    <x-slot name="title">Add Unit</x-slot>
    @if (session()->has('unit_added'))
        <div class="alert alert-success">
            Measurement unit added succesfully.
        </div>
    @endif
    <form wire:submit.prevent="addUnit">
        <div class="mb-3">
            <label for="name" class="form-label">Unit</label>
            <input type="text" wire:model="name" id="unit" class="form-control @error('name') is-invalid @enderror">
            <x-invalid-feedback field="name" />
        </div>
        <div class="d-grid">
            <button class="btn btn-secondary">Add Unit</button>
        </div>
    </form>
</x-info-card>
