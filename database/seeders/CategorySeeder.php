<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name' => 'First Category',
                'slug' => 'first-category'
            ],
            [
                'name' => 'Second Category',
                'slug' => 'second-category'
            ],
            [
                'name' => 'Third Category',
                'slug' => 'third-category'
            ]
        ]);
    }
}