<?php

namespace App\Http\Livewire;

use App\Models\ProductHistory;
use App\Models\StockRequest;
use App\Models\StockTransfer;
use App\Models\Store;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class SingleRequestToStore extends Component
{
    public $request;
    public $store;
    public $messages;
    public function render()
    {
        return view('livewire.single-request-to-store');
    }


    public function mount(StockRequest $stockRequest)
    {

        $this->store = Store::find(auth()->user()->store_id);
        $this->request = $stockRequest;
        $this->messages = collect([]);
        foreach ($stockRequest->requested_items as $item) {
            $stock = $this->store->stocks()->where('product_variation_id', $item['product_variation_id'])->first();
            if (($stock->current_stock - $item['quantity'] <= $stock->stock_threshold)) {
                $this->messages->push('You do not have enough stocks for this request');
            }
        }
    }

    public function approve()
    {
        //Steps to approve

        //1. check if no errors occured
        if ($this->messages->count() === 0) {
            //2. create the transfer
            $transfer = StockTransfer::create([
                'from' => $this->request->answering_store,
                'to' => $this->request->requesting_store,
                'sent_by' => auth()->id(),
                'stock_request_id' => $this->request->id,
            ]);
            foreach ($this->request->requested_items as $item) {
                //3. create tranfer items
                $transfer->items()->create([
                    'product_variation_id' => $item['product_variation_id'],
                    'quantity' => $item['quantity']
                ]);
                //4. subtract tranfer item from store from stock
                $stock = $this->store->stocks()->where('product_variation_id', $item['product_variation_id'])->first();
                $stock->current_stock -= $item['quantity'];
                $stock->save();


                // 5. Log the action
                ProductHistory::create([
                    'variation_id' => $item['product_variation_id'],
                    'quantity' => $item['quantity'],
                    'type' => ProductHistory::SUBTRACT_TYPE,
                    'reason' => 'Transfer to store ' . $this->request->requesting_store,
                    'store_id' => $this->store->id,
                    'user_id' => auth()->id()
                ]);
            }


            //6. approve the stock request
            $this->request->status = StockRequest::APPROVED_STATUS;
            $this->request->save();
            $this->emitSelf('request_approved');
        }
    }

    public function decline()
    {
        $this->request->status = StockRequest::DECLINED_STATUS;
        $this->request->save();
        $this->emitSelf('request_declined');
    }
}