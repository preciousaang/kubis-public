<?php

namespace App\Http\Livewire\Store;

use App\Models\Store;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Livewire\Component;

class AddStore extends Component
{
    use AuthorizesRequests;
    public $name;
    public $address;
    public $isHQ = false;

    protected function getRules()
    {
        return [
            'name' => 'required|unique:stores,name',
            'address' => 'required',

        ];
    }
    public function render()
    {
        return view('livewire.store.add-store');
    }

    public function addStore()
    {
        $this->authorize('create', Store::class);
        $this->validate();
        $store = new Store();
        $store->name = $this->name;
        $store->address = $this->address;
        $store->save();
        session()->flash('store_added', 'Store added');
        $this->resetExcept();
        $this->emitUp('storeAdded');
    }
}