<x-info-card>
    <x-slot name="title">Add Brand</x-slot>
    <x-alert type="success" message="brand_added" />
    <form wire:submit.prevent="addBrand" action="">
        <div class="mb-3">
            <label for="brandName" class="form-label">
                Brand Name
            </label>
            <input type="text" wire:model="name" class="form-control @error('name') is-invalid @enderror">
            <x-invalid-feedback field="name" />
        </div>
        <div class="mb-3">
            <label for="brandContact" class="form-label">
                Brand Contact
            </label>
            <input type="text" wire:model="contact" class="form-control @error('contact') is-invalid @enderror">
            <x-invalid-feedback field="contact" />
        </div>
        <div class="mb-3">
            <label for="brandName" class="form-label">
                Brand Address
            </label>
            <textarea wire:model="address" class="form-control @error('address') is-invalid @enderror"></textarea>
            <x-invalid-feedback field="address" />
        </div>
        <div class="d-grid">
            <button type="submit" class="btn btn-secondary">Add brand</button>
        </div>

</x-info-card>
</form>
