<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory;
    protected $fillable = [
        'order_id',
        'product_variation_id',
        'quantity',
        'price',
        'status',
        'rate',
        'rate_comment'
    ];

    public function variation()
    {
        return $this->belongsTo(ProductVariation::class, 'product_variation_id');
    }

    public function getProductAttribute()
    {
        return $this->variation->product ?? null;
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function getOwnerAttribute()
    {
        return isset($this->order) ? $this->order->user : null;
    }
}