@section('title', 'Forgot Password')
<div class="vh-100 vw-100 bg-info row justify-content-center align-items-center ">
    <div class="col-md-3 col-sm-6 col-12">
        <div class="card shadow">
            <div class="card-header text-center bg-info text-white fw-bold">
                Forgot Password
            </div>
            <div class="card-body">
                <x-alert type="info" message="email_sent" />
                <form wire:submit.prevent="sendResetMail">
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="text" wire:model="email" class="form-control @error('email') is-invalid @enderror">
                        <x-invalid-feedback field="email" />
                    </div>
                    <div class="d-grid">
                        <button type="submit" class="btn btn-info">
                            Send Reset Mail
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
