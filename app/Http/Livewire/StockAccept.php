<?php

namespace App\Http\Livewire;

use Livewire\Component;

class StockAccept extends Component
{
    public function render()
    {
        return view('livewire.stock-accept');
    }
}
