@section('title', 'Reset Password')
<div class="vh-100 vw-100 bg-info row justify-content-center align-items-center ">
    <div class="col-md-3 col-sm-6 col-12">
        <div class="card shadow">
            <div class="card-header text-center bg-info text-white fw-bold">
                Reset Password
            </div>
            <div class="card-body">
                <x-alert type="info" message="reset_done" />
                @error('token')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <form wire:submit.prevent="resetPassword">
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="text" wire:model="email" class="form-control @error('email') is-invalid @enderror">
                        <x-invalid-feedback field="email" />
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" wire:model="password"
                            class="form-control @error('password') is-invalid @enderror">
                        <x-invalid-feedback field="password" />
                    </div>
                    <div class="mb-3">
                        <label for="password_confirmation" class="form-label">Confirm Password</label>
                        <input type="password" wire:model="password_confirmation"
                            class="form-control @error('password_confirmation') is-invalid @enderror">
                        <x-invalid-feedback field="password_confirmation" />
                    </div>
                    <div class="d-grid">
                        <button type="submit" class="btn btn-info">
                            Reset Password
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
