<x-info-card>
    <x-slot name="title">Product Details</x-slot>
    @if ($product)
        <img class="img-fluid rounded" src="{{ asset('storage/uploads/' . $product->image) }}"
            alt="{{ $product->name }}" />
        <br>
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <td class="fw-bold">
                        Product Name
                    </td>
                    <td>{{ $product->name }}</td>
                </tr>
                <tr>
                    <td class="fw-bold">
                        Product Category
                    </td>
                    <td>{{ $product->category->name }}</td>
                </tr>
                <tr>
                    <td class="fw-bold">
                        Product Brand
                    </td>
                    <td>{{ $product->brand->name }}</td>
                </tr>
            </table>
        </div>
    @endif
</x-info-card>
