<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Store;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    //

    public function index()
    {
        //get categories and count
        $categories = Category::orderBy('name')->withCount(['products' => function (Builder $query) {
            $query->whereHas('variations', function (Builder $query) {
                $query->whereHas('stocks', function (Builder $query) {
                    $query->where('store_id', hq_store()->id);
                });
            });
        }])->get();
        return response()->json(['categories' => $categories]);
    }
}