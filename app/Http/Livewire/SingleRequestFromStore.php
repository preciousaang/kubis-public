<?php

namespace App\Http\Livewire;

use App\Models\StockRequest;
use Livewire\Component;

class SingleRequestFromStore extends Component
{
    public $request;
    public function render()
    {
        return view('livewire.single-request-from-store');
    }

    public function mount(StockRequest $stockRequest)
    {
        $this->request = $stockRequest;
    }
}