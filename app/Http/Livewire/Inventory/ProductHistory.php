<?php

namespace App\Http\Livewire\Inventory;

use App\Models\Product;
use Livewire\Component;
use App\Models\ProductHistory as History;
use Livewire\WithPagination;

class ProductHistory extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $product;
    public function render()
    {

        $histories =  $this->product->histories()->latest()->paginate(20);

        return view('livewire.inventory.product-history', ['histories' => $histories]);
    }

    public function mount(Product $product)
    {

        $this->product = $product;
    }
}