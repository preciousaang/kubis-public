<?php

use App\Models\Store;

if (!function_exists('hq_store')) {
    function hq_store()
    {
        $hqStore = Store::where('is_headquarters', true)->first();
        return $hqStore;
    }
}