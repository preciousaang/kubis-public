<?php

namespace App\Http\Livewire\Configuration;

use App\Models\Unit;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithPagination;

class Units extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $selectedUnit;
    public $search;

    protected $listeners = [
        'unitAdded' => 'onUnitAdded'
    ];

    protected function getRules()
    {
        return [
            'selectedUnit.name' => ['required', 'string', 'max:50', Rule::unique('units', 'name')->ignore($this->selectedUnit->name)],
        ];
    }

    public function onUnitAdded()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        if (!$this->search) {
            $units = Unit::orderBy('name', 'asc');
        } else {

            $units  = Unit::where('name', 'like', '%' . $this->search . '%')->orderBy('name', 'asc');
        }
        $units = $units->paginate(20);

        return view('livewire.configuration.units', [
            'units' => $units
        ]);
    }


    public function selectUnit(Unit $unit)
    {
        $this->selectedUnit = $unit;
    }


    public function onEdit()
    {
        $this->selectedUnit->save();
        $this->emit('unitEdited');
        session()->flash('unit_edited', 'Unit Edited Succesfully');
    }

    public function onDelete()
    {
        $this->selectedUnit->delete();
        $this->selectedUnit = null;
        $this->emit('unitDeleted');
        session()->flash('unit_deleted', 'Unit Deleted Successfully');
    }
}
