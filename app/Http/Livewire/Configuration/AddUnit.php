<?php

namespace App\Http\Livewire\Configuration;

use App\Models\Unit;
use Livewire\Component;


class AddUnit extends Component
{
    public $name;

    protected $rules = [
        'name' => 'required|string|max:50|unique:units'
    ];

    public function render()
    {
        return view('livewire.configuration.add-unit');
    }

    public function addUnit()
    {
        $this->validate();
        $unit = new Unit();
        $unit->name = $this->name;
        $unit->save();
        $this->name = null;
        $this->emitUp('unitAdded');
        session()->flash('unit_added', true);
    }
}
