<?php


namespace Kubis\Services\Sales;

use App\Models\ProductVariation;
use Illuminate\Support\Facades\DB;
use App\Models\Store;
use Illuminate\Support\Facades\Log;
use App\Models\Sale;
use Illuminate\Support\Str;
use App\Models\Stock;
use Exception;

class SalesService implements SalesServiceInterface
{
    public function doSale($products, $paymentType, $purchaseType, $user)
    {
        return DB::transaction(function () use ($products, $paymentType, $purchaseType,  $user) {
            $store = Store::find($user->store_id);
            $sale = Sale::create([
                'store_id' => $store->id,
                'unique_id' => Str::uuid(),
                'purchase_type' => Sale::LOCAL_PURCHASE_TYPE,
                'payment_type' => $paymentType,
                'purchase_type' => $purchaseType,
                'user_id' => $user->id,
            ]);



            foreach ($products as $product) {
                $storeStock = $store
                    ->stocks()
                    ->where('product_variation_id', $product['product_variation_id'])
                    ->first();

                $saleItem = $sale->items()->create([
                    'product_variation_id' => $product['product_variation_id'],
                    'quantity' => $product['quantity'],
                    'amount' => $storeStock
                        ->selling_price
                ]);

                if ($storeStock->cost_per_unit < $storeStock->selling_price) {
                    $saleItem->profit = (int)$product['quantity'] * ($storeStock->selling_price - $storeStock->cost_per_unit);
                    $saleItem->save();
                } else {
                    $saleItem->loss = (int)$product['quantity'] * ($storeStock->cost_per_unit - $storeStock->selling_price);
                    $saleItem->save();
                }

                $stock  = Stock::where('product_variation_id', $product['product_variation_id'])
                    ->where('store_id', $store->id)
                    ->first();
                if ($stock === null) {
                    throw new Exception("Stock does not exist");
                } else {
                    $stock->current_stock = $stock->current_stock - $product['quantity'];
                    $stock->save();
                }
            }

            return $sale;
        });
    }

    public function singleSale($unique_id): ?Sale
    {
        return Sale::where('unique_id', $unique_id)->first();
    }

    public function storeSales($storeId, $perPage = 10, $opt = [])
    {
        $sales = Sale::where('store_id', $storeId);
        $sales = $sales->paginate($perPage);
        return $sales;
    }
}