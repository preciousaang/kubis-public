<div wire:ignore.self class="modal fade" id="{{ $id }}"
    {{ $dismissable == true ? '' : 'data-bs-backdrop="static"' }} tabindex="-1" aria-labelledby="{{ $id }}"
    aria-hidden="true">

    <div class="modal-dialog {{ isset($size) ? 'modal-' . $size : '' }}">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ $title ?? null }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                {{ $slot }}
            </div>
        </div>
    </div>

</div>
