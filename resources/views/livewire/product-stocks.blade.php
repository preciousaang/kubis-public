@section('title', 'Product stocks')
<div>
    @livewire('nav-bar')
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">
                    AVAILABLE PRODUCT STOCKS
                </h2>
                <h6 class="card-subtitle">
                    List of Saleable Products
                </h6>
                <div class="d-flex mt-2 mb-3 justify-content-between">
                    <div class="d-flex align-items-center gap-2">
                        Show
                        <select name="" wire:model="entries" id="" class="form-select form-select-sm">
                            @for ($x = 10; $x <= 100; $x = $x + 10)
                                <option value="{{ $x }}">
                                    {{ $x }}
                                </option>
                            @endfor
                        </select>
                        entries
                    </div>
                    @if (auth()->user()->is_admin)
                        <div>
                            <select name="" wire:model="selected_store_id" id="" class="form-select">
                                @forelse($stores as $store)
                                    <option value="{{ $store->id }}">
                                        {{ $store->name }}{{ $store->is_headquarters ? ' - (Headquarters)' : null }}
                                    </option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                    @endif
                    <div class="d-flex align-items-center gap-2">
                        <input type="text" name="" id="" class="form-control form-control-sm"
                            wire:model.debounce.500ms="search" placeholder="Search">
                        <i wire:loading wire:target="search" class="spinner-border spinner-border-sm"></i>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-center">
                        <thead class="bg-info">
                            <tr>
                                <th>SKU</th>
                                <th>Name</th>
                                <th>Variation Name</th>
                                <th>Category</th>
                                <th>Available Stock</th>
                                <th>Price</th>

                            </tr>
                        </thead>
                        <tbody>
                            @forelse($stocks as $stock)
                                <tr>
                                    <td>{{ $stock->variation->SKU }}</td>
                                    <td>{{ isset($stock->variation) ? $stock->variation->product->name : null }}</td>
                                    <td>{{ isset($stock->variation) ? $stock->variation->name : null }}</td>
                                    <td>{{ isset($stock->variation) ? $stock->variation->product->category->name : null }}
                                    </td>
                                    <td>{{ isset($stock->variation) ? $stock->current_stock : null }}</td>
                                    <td>{{ isset($stock->variation) ? __('₦') . $stock->selling_price : null }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6">No stocks yet</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="d-flex justify-content-end">
                    {{ $stocks->links() }}
                </div>
            </div>
            <div class="card-footer">
                <button wire:click="exportPDF" class="btn btn-outline-info btn-sm" type="button">
                    Export spreadsheet
                </button>
            </div>
        </div>
    </div>

</div>
