<?php

namespace App\Http\Livewire;

use App\Models\Store;
use Livewire\Component;

class Dashboard extends Component
{
    public $user, $store;
    public function render()
    {
        return view('livewire.dashboard');
    }

    public function mount()
    {
        $this->user = auth()->user();
        if (!$this->user->is_admin) {
            $store = Store::with(['sales', 'sales.items'])->where('id', $this->user->store_id)->first();
            $this->store = $store;
        }
    }
}