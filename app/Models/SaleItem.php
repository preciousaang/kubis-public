<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'sale_id',
        'product_variation_id',
        'quantity',
        'amount',
    ];

    public function sale()
    {
        return $this->belongsTo(Sale::class, 'sale_id');
    }

    public function variation()
    {
        return  $this->belongsTo(ProductVariation::class, 'product_variation_id');
    }

    public function getProductAttribute()
    {
        $this->variation->product;
    }
}