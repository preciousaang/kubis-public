@section('title', 'Manage Product Stock')
<div>
    @livewire('nav-bar')
    <div class="row container-fluid gy-3">
        <div class="col-md-4">
            @livewire('inventory.manage-product-details', ['product'=>$product])
        </div>
        <div class="col-md-8">
            <h3>Product Variations</h3>
            <div class="d-flex my-2">
                @can('create', App\Models\ProductVariation::class)
                    <a href="#" class="btn btn-outline-primary btn-sm" data-bs-toggle="modal"
                        data-bs-target="#addVariationModal">
                        <i class="fa fa-plus"></i> &nbsp; Add new variation
                    </a>
                @endcan
            </div>
            <x-alert message="variation_added" type="success" />
            <x-alert message="variation_edited" type="success" />
            <div class="row gy-3">
                @forelse($product->variations as $variation)
                    @livewire('inventory.manage-product-variation-detail', ['variation'=>$variation],
                    key($variation->id))
                @empty
                @endforelse
            </div>
        </div>
    </div>
    @can('create', App\Models\ProductVariation::class)
        <x-modal id="addVariationModal" size="lg" :dismissable="false">
            <x-slot name="title">Add Product Variation</x-slot>
            <form wire:submit.prevent="addVariation">
                <div class="row gy-2">

                    @if (isset($variationImage))
                        <div class="col-12">
                            <img style="height: 100px" class="img-fluid rounded"
                                src="{{ $variationImage->temporaryUrl() }}" />
                        </div>
                    @endif
                    <div class="col-6">
                        <div class="mb-2">
                            <label for="variationImage" class="form-label">Variation Image</label>
                            <input class="form-control @error('variationImage') is-invalid @enderror" type="file"
                                accept="image/*" wire:model="variationImage">
                            <x-invalid-feedback field="variationImage" />
                        </div>
                        <div class="mb-2">
                            <label for="variationName" class="form-label">Variation Name</label>
                            <input class="form-control @error('variationName') is-invalid @enderror" type="text"
                                wire:model="variationName">
                            <x-invalid-feedback field="variationName" />
                        </div>
                        <div class="mb-2">
                            <label for="currentStock" class="form-label">Current Stock</label>
                            <input class="form-control @error('currentStock') is-invalid @enderror" type="text"
                                wire:model="currentStock">
                            <x-invalid-feedback field="currentStock" />
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-2">
                            <label for="variationMeasurementValue" class="form-label">Measurement Value</label>
                            <input class="form-control @error('measurementValue') is-invalid @enderror" type="text" name=""
                                id="variationMeasurementValue" wire:model="measurementValue">
                            <x-invalid-feedback field="measurementValue" />
                        </div>
                        <div class="mb-2">
                            <label for="measurementUnit" class="form-label">Measurement Unit</label>
                            <select wire:model="variationMeasurementUnit" id="measurementUnit"
                                class="form-select @error('variationMeasurementUnit') is-invalid @enderror">
                                <option selected>Select Unit</option>
                                @forelse($units as $unit)
                                    <option value="{{ $unit->name }}">{{ $unit->name }}</option>
                                @empty
                                @endforelse
                            </select>
                            <x-invalid-feedback field="variationMeasurementUnit" />
                        </div>

                        <div class="mb-2">
                            <label for="stockThreshold" class="form-label">Stock Threshold</label>
                            <input class="form-control @error('stockThreshold') is-invalid @enderror" type="text"
                                wire:model="stockThreshold">
                            <x-invalid-feedback field="stockThreshold" />
                        </div>
                    </div>

                    <div class="col-12">
                        @forelse($stores as $store)
                            <div class="form-check form-check-inline">
                                <input class="form-check-input @error('publishedStores.*') is-invalid @enderror"
                                    type="checkbox" wire:model="publishedStores" id="store-{{ $store->id }}"
                                    value="{{ $store->id }}">
                                <label class="form-check-label" for="store-{{ $store->id }}">{{ $store->name }}
                                    @if ($store->is_headquarters)
                                        <span class="text-success">(Headquarters)</span>
                                    @endif
                                </label>
                                <x-invalid-feedback field="publishedStore.*" />
                            </div>
                        @empty
                            You need to add store
                        @endforelse
                        @error('publishedStores')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-6">
                        <div class="mb-2">
                            <label for="" class="form-label">Cost Per Unit</label>
                            <input type="text" name="" id="" wire:model="cost_per_unit"
                                class="form-control @error('cost_per_unit') is-invalid @enderror">
                            <x-invalid-feedback field="cost_per_unit" />
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-2">
                            <label for="" class="form-label">Selling Price</label>
                            <input type="text" name="" id="" wire:model="selling_price"
                                class="form-control @error('selling_price') is-invalid @enderror">
                            <x-invalid-feedback field="selling_price" />
                        </div>
                    </div>

                    <div class="col-12">
                        <button type="submit" class="btn btn-info">
                            Add Variation
                        </button>
                    </div>

                </div>
            </form>

        </x-modal>
    @endcan
    <x-modal id="viewVariationModal" size="lg">
        @if ($selectedVariation)
            <x-slot name="title">View Variation</x-slot>

            <div class="row">
                <div class="col-md-5">
                    <img class="img-fluid rounded" style="max-height: 300px;"
                        src="{{ asset('storage/uploads/' . $selectedVariation->image) }}"
                        alt="{{ $selectedVariation->name }}">
                </div>
                <div class="col-md-7">
                    <h3 class="card-title">
                        {{ $selectedVariation->name }}
                    </h3>
                    <table class="table">
                        <tbody>
                            <tr>
                                <td class="fw-bold">SKU</td>
                                <td> {{ $selectedVariation->sku }}</td>
                            </tr>
                            <tr>
                                <td class="fw-bold">Available Stock</td>
                                <td> {{ auth()->user()->is_admin
                                    ? $selectedVariation->stock_count
                                    : $selectedVariation->stocks()->where('store_id', auth()->user()->store_id)->sum('current_stock') }}
                                </td>
                            </tr>
                            <tr>
                                <td class="fw-bold">Size</td>
                                <td> {{ $selectedVariation->size }}</td>
                            </tr>

                        </tbody>
                    </table>

                    {!! DNS2D::getBarcodeHTML($selectedVariation->sku, 'QRCODE') !!}

                </div>
            </div>
        @else
            <h1 class="card-title text-center">
                Loading.....
            </h1>
        @endif
    </x-modal>
    @can('create', App\Models\ProductVariation::class)
        <x-modal id="editVariationModal" size="lg" :dismissable="false">
            <x-slot name="title">Edit Product Variation</x-slot>
            <form wire:submit.prevent="editVariation">
                <div class="row gy-2">
                    @if (isset($variationImage) || isset($selectedVariation->image))
                        <div class="col-12">
                            <img style="height: 100px" class="img-fluid rounded"
                                src="{{ isset($variationImage)? $variationImage->temporaryUrl(): asset('storage/uploads/' . $selectedVariation->image) }}" />
                        </div>
                    @endif
                    <div class="col-6">
                        <div class="mb-2">
                            <label for="variationImage" class="form-label">Variation Image</label>
                            <input class="form-control @error('variationImage') is-invalid @enderror" type="file"
                                accept="image/*" wire:model="variationImage">
                            <x-invalid-feedback field="variationImage" />
                        </div>
                        <div class="mb-2">
                            <label for="variationName" class="form-label">Variation Name</label>
                            <input class="form-control @error('selectedVariation.name') is-invalid @enderror" type="text"
                                wire:model="selectedVariation.name">
                            <x-invalid-feedback field="selectedVariation.name" />
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-2">
                            <label for="variationMeasurementValue" class="form-label">Measurement Value</label>
                            <input class="form-control @error('selectedVariation.measurement_value') is-invalid @enderror"
                                type="text" name="" id="variationMeasurementValue"
                                wire:model="selectedVariation.measurement_value">
                            <x-invalid-feedback field="selectedVariation.measurement_value" />
                        </div>
                        <div class="mb-2">
                            <label for="measurementUnit" class="form-label">Measurement Unit</label>
                            <select wire:model="selectedVariation.measurement_unit" id="measurementUnit"
                                class="form-select @error('selectedVariation.measurement_unit') is-invalid @enderror">
                                <option selected>Select Unit</option>
                                @forelse($units as $unit)
                                    <option value="{{ $unit->name }}">{{ $unit->name }}</option>
                                @empty
                                @endforelse
                            </select>
                            <x-invalid-feedback field="selectedVariation.measurement_value" />
                        </div>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-info">
                            Edit Variation
                        </button>
                    </div>
                </div>
            </form>
        </x-modal>
    @endcan
    <script>
        document.addEventListener('livewire:load', () => {

            document.addEventListener('DOMContentLoaded', () => {

                const addVariationModalEl = selectId('addVariationModal')
                const addVariationModal = new Modal(addVariationModalEl);
                const editVariationModalEl = selectId('editVariationModal')
                const editVariationModal = new Modal(editVariationModalEl);
                const viewVariationModalEl = selectId('viewVariationModal');
                const viewVariationModal = new Modal(viewVariationModalEl);
                Livewire.on('variationSaved', () => {

                    addVariationModal.hide();
                    editVariationModal.hide();

                })
                viewVariationModalEl.addEventListener('hidden.bs.modal', () => {
                    @this.deselectVariation();
                });

                addVariationModalEl.addEventListener('hidden.bs.modal', () => {
                    @this.deselectVariation();
                });
                editVariationModalEl.addEventListener('hidden.bs.modal', () => {
                    @this.deselectVariation();
                });
            })

        })
    </script>
</div>
