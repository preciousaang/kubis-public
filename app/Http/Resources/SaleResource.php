<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SaleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'unique_id' => $this->unique_id,
            'seller' => new AuthUserResource($this->seller),
            'items' => SaleItemResource::collection($this->items),
            'total_amount' => $this->items()->sum('amount'),
            'date' => $this->created_at,
        ];
    }
}