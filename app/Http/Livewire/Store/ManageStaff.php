<?php

namespace App\Http\Livewire\Store;

use Livewire\Component;

class ManageStaff extends Component
{
    public function render()
    {
        return view('livewire.store.manage-staff');
    }
}
