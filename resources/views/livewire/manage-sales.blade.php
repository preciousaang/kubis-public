@section('title', 'Manage Sales')
<div>
    @livewire('nav-bar')
    <div class="container-fluid">
        <div class="row gy-2">
            <div class="col-md-4">
                <x-info-card>
                    <x-slot name="title">
                        Filter Sales
                    </x-slot>
                    <form wire:submit.prevent="filterByDate">
                        <div class="h4 text-secondary">Date</div>
                        <div class="mb-2">
                            <label for="from" class="form-label">From</label>
                            <input type="date" wire:model.defer="from"
                                class="form-control @error('from') is-invalid @enderror">
                            <x-invalid-feedback field="from" />
                        </div>
                        <div class="mb-2">
                            <label for="to" class="form-label">To</label>
                            <input type="date" wire:model.defer="to"
                                class="form-control @error('to') is-invalid @enderror">
                            <x-invalid-feedback field="to" />
                        </div>
                        <div class="d-grid">
                            <button class="btn btn-sm btn-success" type="submit">Filter</button>
                        </div>
                    </form>
                </x-info-card>

            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-center h5">
                        Manage Sales
                    </div>
                    <div class="card-body">
                        <div class="d-flex my-2 justify-content-between">
                            <div class="form-group">
                                <label for="entires" class="form-label">Entries</label>
                                <select wire:model="per_page" class="form-select form-select-sm" name="" id="entries">
                                    @for ($i = 10; $i <= 100; $i += 10)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>



                                </select>
                            </div>

                            <button wire:click="exportSales" class="btn btn-sm btn-info">Export to Excel</button>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-center">
                                <thead>
                                    <tr>
                                        <th>
                                            SKU
                                        </th>
                                        <th>
                                            Product Name
                                        </th>
                                        <th>Variation</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($sales as $sale)
                                        <tr>
                                            <td>{{ $sale->variation->sku }}</td>
                                            <td>{{ $sale->variation->product->name }}</td>
                                            <td>{{ $sale->variation->name }}</td>
                                            <td>{{ $sale->quantity }}</td>
                                            <td>&#8358;{{ $sale->amount }}</td>
                                            <td>{{ $sale->created_at->toDateTimeString() }}</td>
                                        </tr>
                                    @empty
                                    @endforelse
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="card-footer">
                        {{ $sales->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
