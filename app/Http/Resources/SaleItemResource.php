<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SaleItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sale_id' => $this->sale_id,
            'product_variation_id' => $this->product_variation_id,
            'quantity' => $this->quantity,
            'amount' => $this->amount,
            'discount' => $this->discount,
            'variation' => new VariationResource($this->variation)
        ];
    }
}