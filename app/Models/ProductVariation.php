<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use BinaryCats\Sku\Concerns\SkuOptions;
use BinaryCats\Sku\HasSku;

class ProductVariation extends Model
{
    use HasFactory, HasSlug, HasSku;


    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }


    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function getAvailableStockAttribute()
    {
        return $this->current_stock - $this->reserved_stock;
    }

    public function getSizeAttribute()
    {
        return $this->measurement_value . ' ' . $this->measurement_unit;
    }

    public function skuOptions(): SkuOptions
    {
        return SkuOptions::make()
            ->from(['name'])
            ->target('sku')
            ->using('-')
            ->forceUnique(true)
            ->generateOnCreate(true)
            ->refreshOnUpdate(false);
    }


    public function stocks()
    {
        return $this->hasMany(Stock::class, 'product_variation_id');
    }


    public function hqStock()
    {
        $store = Store::where('is_headquarters', true)->first();
        return $this->hasOne(Stock::class, 'product_variation_id')->where('store_id', $store->id);
    }

    public function getStockCountAttribute()
    {
        return $this->stocks()->sum('current_stock');
    }

    public function orders()
    {
        return $this->hasMany(OrderItem::class);
    }
}