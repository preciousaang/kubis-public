<?php

use App\Http\Livewire\ChangePassword;
use App\Http\Livewire\Configuration\Units;
use App\Http\Livewire\Dashboard;
use App\Http\Livewire\ForgotPassword;
use App\Http\Livewire\Inventory\AssignStocks;
use App\Http\Livewire\Inventory\ManageBrands;
use App\Http\Livewire\Inventory\ManageProduct;
use App\Http\Livewire\Inventory\ManageStock;
use App\Http\Livewire\Inventory\ProductHistory;
use App\Http\Livewire\InventoryCategories;
use App\Http\Livewire\InventoryProducts;
use App\Http\Livewire\Login;
use App\Http\Livewire\ManageSales;
use App\Http\Livewire\ManageUsers;
use App\Http\Livewire\Store\ManageStore;
use App\Http\Livewire\ProductStocks;
use App\Http\Livewire\RequestStocks;
use App\Http\Livewire\ResetPassword;
use App\Http\Livewire\Sales\ManageOrder;
use App\Http\Livewire\Sales\ManageOrders;
use App\Http\Livewire\SingleRequestFromStore;
use App\Http\Livewire\StockRequestsFromStore;
use App\Http\Livewire\SingleRequestToStore;
use App\Http\Livewire\StockRequestsToStore;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// TODO: create api routes for orders
// TODO: create api routes for POS
// TODO: create api routes for SALES
// TODO: create api routes for orders
// TODO: create api routes for orders
// TODO: create api routes for orders
// TODO: create api routes for orders
// TODO: create api routes for orders


Route::middleware('guest')->group(function () {
    Route::get('/login', Login::class)->name('login');
});

Route::get('/forgot-password', ForgotPassword::class)->middleware('guest')->name('password.request');
Route::get('/reset-password/{token}', ResetPassword::class)->middleware('guest')->name('password.reset');

// ================================= Auth Routes ===============================================

Route::middleware('auth')->group(function () {
    // =============================== Inventory Routes =======================================

    Route::get('/', Dashboard::class)->name('dashboard');
    Route::get('/change-password', ChangePassword::class)->name('password.change');
    Route::prefix('/inventory')->group(function () {
        Route::get('/categories', InventoryCategories::class)->name('inventory-categories');
        Route::get('/products', InventoryProducts::class)->name('inventory-products');
        Route::get('/manage-product/{slug}', ManageProduct::class)->name('manage-product');
        Route::get('/product-stocks', ProductStocks::class)->name('inventory-product-stocks');
        Route::get('/manage-stock', ManageStock::class)->name('inventory-manage-stock');
        Route::get('/manage-brands', ManageBrands::class)->name('inventory-manage-brands');
        Route::get('/assign-stocks/{slug}', AssignStocks::class)->name('inventory-assign-stocks');
        Route::get('/product-history/{product}', ProductHistory::class)->name('inventory-product-history');
        Route::get('/request-stocks/{product}', RequestStocks::class)->name('inventory-request-stocks');
        Route::get('/stocks-requests-from-store', StockRequestsFromStore::class)->name('inventory-requests-from-store');
        Route::get('/stocks-requests-to-store', StockRequestsToStore::class)->name('inventory-requests-to-store');
        Route::get('/stocks-requests-from-store/{stockRequest}', SingleRequestFromStore::class)->name('single-request-from-store');
        Route::get('/stocks-requests-to-store/{stockRequest}', SingleRequestToStore::class)->name('single-request-to-store');
    });



    Route::prefix('/sales')->group(function () {
        // =============================== Sales Routes =======================================
        Route::get('/manage-sales', ManageSales::class)->name('manage-sales');




        // =============================== Orders Routes =======================================
        Route::get('/orders', ManageOrders::class)->name('manage-orders');
        Route::get('/order/{order}', ManageOrder::class)->name('manage-order');
    });



    // =============================== POS Routes =======================================



    //  // =============================== Auth Routes =======================================


    // =============================== Permission Routes =======================================
    Route::get('/manage-users', ManageUsers::class)->name('manage-users');



    // =============================== Configuration Routes =======================================

    Route::prefix('/configuration')->group(function () {
        Route::get('/units', Units::class)->name('configuration-units');
        Route::get('/manage-stores', ManageStore::class)->name('manage-stores');
    });
});