<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use Livewire\WithPagination;

class StockRequestsToStore extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';



    public $user;
    public $store;

    public function render()
    {
        $requests =  $this->store->stockAsks()->paginate(20);
        return view('livewire.stock-requests-to-store', [
            'stockRequests' => $requests,
        ]);
    }

    public function mount(User $user)
    {
        $this->user = $user->find(auth()->id());
        $this->store = $this->user->store;
    }
}