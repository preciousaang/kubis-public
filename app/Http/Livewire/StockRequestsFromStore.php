<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class StockRequestsFromStore extends Component
{
    public $user;
    public $store;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public function render()
    {
        return view('livewire.stock-requests-from-store', [
            'stockRequests' => $this->store->stockRequests()->paginate(20)
        ]);
    }


    public function mount(User $user)
    {
        $this->user = $user->find(auth()->id());
        $this->store = $this->user->store;
    }
}