<?php

namespace App\Http\Livewire;

use App\Models\Product;
use App\Models\ProductVariation;
use App\Models\StockRequest;
use App\Models\Store;

use Livewire\Component;

class RequestStocks extends Component
{


    public $stores;
    public $product;
    public $store;
    public $requestedStocks;
    //TODO make the product listed a table
    //TODO dissapear product when added to requested list
    protected $listeners = ['addVariationAndCount'];



    protected function getRules()
    {
        return [
            'store' => 'required|exists:stores,id'
        ];
    }


    public function render()
    {
        return view('livewire.request-stocks');
    }

    public function mount(Product $product)
    {
        $this->product = $product;
        $this->stores = Store::where('id', '<>', auth()->user()->store_id)->get();
        $this->requestedStocks = collect([]);
    }

    public function addVariationAndCount(ProductVariation $variation, $quantity)
    {

        if ($this->requestedStocks->whereStrict('variation.id', $variation->id)->count() === 0) {
            $this->requestedStocks
                ->push([
                    'variation' => $variation,
                    'quantity' => $quantity
                ]);
        } else {
            $index = $this->requestedStocks->search(function ($item, $key) use ($variation, $quantity) {
                return $item['variation']['id'] === $variation->id;
            });
            $this->requestedStocks->put($index, ['variation' => $variation, 'quantity' => $quantity]);
        }
    }

    public function updatedRequested()
    {
        $this->resetErrorBag('requested');
    }

    public function requestProduct()
    {
        $this->validate();
        if ($this->requestedStocks->count() <= 0) {
            $this->addError('requestedStocks', 'You need to select products');
            return;
        }


        $stockRequest = StockRequest::create([
            'requesting_store' => auth()->user()->store_id,
            'answering_store' => $this->store,
            'requested_by' => auth()->id(),

        ]);

        foreach ($this->requestedStocks->all() as $item) {
            $stockRequest->requested_items()->create([
                'product_variation_id' => $item['variation']['id'],
                'quantity' => $item['quantity']
            ]);
        }
        $this->reset('store');
        $this->requestedStocks = collect([]);
        $this->resetErrorBag();
        session()->flash('request_sent', 'Your request has been sent');
        $this->emitTo('request-stock-item', 'resetItems');
    }

    public function removeStock($id)
    {
        $index = $this->requestedStocks->search(function ($item, $key) use ($id) {
            return $item['variation']['id'] === $id;
        });

        $this->requestedStocks->splice($index, 1);
    }
}