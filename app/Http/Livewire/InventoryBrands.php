<?php

namespace App\Http\Livewire;

use Livewire\Component;

class InventoryBrands extends Component
{
    public function render()
    {
        return view('livewire.inventory-brands');
    }
}
