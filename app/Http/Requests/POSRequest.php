<?php

namespace App\Http\Requests;

use App\Models\ProductVariation;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

class POSRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('operate-pos');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'products' => 'required|array',
            'payment_type' => 'required|in:card,cash,transfer',
            'products.*.product_variation_id' => function ($attribute, $value, $fail) {

                $variation = ProductVariation::find($value);
                if (!$variation) {
                    $fail('Invalid variation');
                    return;
                }
                $stock = $variation->stocks()->where('store_id', $this->user()->store_id);

                if (!$stock->exists()) {
                    $fail("Invalid product");
                }
            },
            'products.*.quantity' => ['required', 'integer', function ($attribute, $value, $fail) {
                // Log::alert($attribute);
                $variation_id = $this->products[explode('.', $attribute)[1]]['product_variation_id'];
                $variation = ProductVariation::find($variation_id);
                if (!$variation) {
                    $fail('Invalid variation');
                    return;
                }
                $stock = $variation->stocks()->where('store_id', $this->user()->store_id)->first();
                if ($stock->current_stock < $value) {
                    $fail('We do not have up to that quantity');
                }
            }],


        ];
    }
}