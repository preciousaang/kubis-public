<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Models\ProductVariation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    //
    public function index(Request $request)
    {
        $products = Product::with(['category'])->whereHas('variations', function (Builder $query) {
            $query->whereHas('stocks', function (Builder $query) {
                $query->where('store_id', hq_store()->id);
            });
        });

        if ($request->category_id) {
            $products = $products->where('category_id', $request->category_id);
        }


        if ($request->search) {
            $products = $products->where('name', 'like', "%{$request->search}%");
        }

        if ($request->per_page && is_int($request->per_page)) {
            $products = $products->paginate($request->per_page);
        } else {
            $products = $products->paginate(30);
        }


        return response()->json(['products' => $products]);
    }

    public function single($slug)
    {
        // TODO fix stock count issue
        $product = Product::with([
            'variations' => function ($query) {
                $query->whereHas('stocks', function ($query) {
                    $query->where('store_id', hq_store()->id);
                });
            }
        ])->withSum(['stocks' => function (Builder $query) {
            $query->where('store_id', hq_store()->id);
        }], 'current_stock')
            ->where('slug', $slug)
            ->first();


        return response()->json(['product' => new ProductResource($product)]);
    }

    public function singleVariation($sku)
    {

        //TODO fix the single variation endpoint

        $variation = ProductVariation::with(['stocks' => function ($query) {
            $query->where('store_id', auth()->user()->store_id)->first();
        }])
            ->where('sku', $sku)
            ->first();
        return [
            'variation' => $variation
        ];
    }
}