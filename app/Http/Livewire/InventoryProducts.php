<?php

namespace App\Http\Livewire;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class InventoryProducts extends Component
{
    use AuthorizesRequests;
    use WithFileUploads;
    use WithPagination;

    public $search;

    protected $paginationTheme = 'bootstrap';
    public $categories = [];
    public $brands = [];
    public $selectedProduct;
    public $image;

    protected $rules = [
        'selectedProduct.name' => 'required|string|max:255',
        'selectedProduct.category_id' => 'required|exists:categories,id',
        'selectedProduct.brand_id' => 'required|exists:brands,id',
        'image' => 'nullable|image|max:2048',
    ];

    protected $listeners = ['productAdded'];

    public function render()
    {
        $products =  Product::with(['category', 'brand']);
        if ($this->search) {
            $products  = $products->where('name', 'like', '%' . $this->search . '%');
        }
        $products = $products->orderBy('name', 'asc')->paginate(20);

        return view('livewire.inventory-products', [
            'products' => $products
        ]);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }



    public function editProduct()
    {
        $this->authorize('create', Product::class);
        $this->validate();

        if ($this->image) {
            $this->selectedProduct->image = basename($this->image->store('public/uploads'));
        }

        $this->selectedProduct->save();
        $this->emit('productEdited');
        session()->flash('product_updated', 'Product Updated Successfully');
        $this->resetExcept(['brands', 'categories']);
    }

    public function selectProduct(Product $product)
    {
        $this->selectedProduct = $product;
    }


    public function mount()
    {
        $this->categories = Category::orderBy('name', 'asc')->get();
        $this->brands = Brand::orderBy('name', 'asc')->get();
    }


    public function productAdded()
    {
        $this->resetPage();
    }
}