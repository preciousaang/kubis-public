<?php

namespace App\Http\Livewire\Inventory;

use App\Models\Product;
use App\Models\ProductVariation;
use App\Models\Store;
use App\Models\Unit;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithFileUploads;

class ManageProduct extends Component
{
    use AuthorizesRequests;
    use WithFileUploads;

    //required $fields;
    public $product;
    public $units;
    public $selectedVariation;
    public $stores;


    // form fields
    public $variationName;
    public $variationImage;
    public $variationSKU;
    public $variationMeasurementUnit;
    public $measurementValue;
    public $currentStock;
    public $stockThreshold;
    public $selling_price;
    public $cost_per_unit;
    public $publishedStores = [];

    protected $listeners = ['onSelect' => 'selectVariation'];

    public function render()
    {
        return view('livewire.inventory.manage-product');
    }


    public function mount(string $slug)
    {
        $this->product = Product::with(['category', 'brand', 'variations', 'variations.stocks' => function ($query) {
            if (!auth()->user()->is_admin) {
                $query->where('store_id', auth()->user()->store_id);
            }
        }])->where('slug', $slug)->first();
        $this->units = Unit::orderBy('name', 'asc')->get();
        $this->stores = Store::orderBy('is_headquarters', 'desc')->get();
    }



    protected function getRules()
    {
        if (isset($this->selectedVariation)) {
            $rules = [
                'selectedVariation.name' => [
                    'required', 'string', 'max:50',
                    Rule::unique('product_variations', 'name')
                        ->where('product_id', $this->product->id)
                        ->ignore($this->selectedVariation->id)
                ],
                'variationImage' => 'nullable|image|max:2048',
                'selectedVariation.measurement_unit' => 'required|string|exists:units,name',
                'selectedVariation.measurement_value' => 'required|numeric',

            ];
        } else {
            $rules = [
                'variationName' => ['required', 'string', 'max:50', Rule::unique('product_variations', 'name')->where('product_id', $this->product->id)],
                'variationImage' => 'required|image|max:2048',
                'stockThreshold' => 'required|integer',
                'currentStock' => 'required|integer',
                'variationMeasurementUnit' => 'required|string|exists:units,name',
                'measurementValue' => 'required|numeric',
                'publishedStores' => 'required',
                'publishedStores.*' => 'exists:stores,id',
                'selling_price' => 'required|numeric|gte:cost_per_unit',
                'cost_per_unit' => 'required|numeric'

            ];
        }


        return $rules;
    }


    public function addVariation()
    {

        //TODO add price per unit field
        $this->authorize('create', ProductVariation::class);
        $this->validate();
        $variation = new ProductVariation();
        $variation->product_id = $this->product->id;
        $variation->name = $this->variationName;
        $variation->image = basename($this->variationImage->store('public/uploads'));
        $variation->measurement_value = $this->measurementValue;
        $variation->measurement_unit = $this->variationMeasurementUnit;

        $variation->save();
        //create variation for all marked stores
        foreach ($this->publishedStores as $store) {
            $variation->stocks()->create([
                'store_id' => $store,
                'current_stock' => $this->currentStock,
                'stock_threshold' => $this->stockThreshold,
                'selling_price' => $this->selling_price,
                'cost_per_unit' => $this->cost_per_unit
            ]);
        }

        $this->emit('variationSaved');
        session()->flash('variation_added', 'Variation added successfully');
        $this->resetErrorBag();
        $this->resetExcept(['units', 'product', 'stores']);
    }

    public function selectVariation(ProductVariation $variation)
    {
        $this->selectedVariation = $variation;
    }

    public function deselectVariation()
    {
        unset($this->selectedVariation);
        $this->resetErrorBag();
    }

    public function editVariation()
    {
        $this->authorize('create', ProductVariation::class);
        $this->validate();
        if ($this->variationImage) {
            $this->selectedVariation->image = basename($this->variationImage->store('public/uploads'));
        }
        $this->selectedVariation->save();
        $this->emit('variationSaved');
        $this->resetErrorBag();
        $this->resetExcept(['units', 'product', 'stores']);
        session()->flash('variation_edited', 'Variation edit successfully');
    }
}