<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VerificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('sanctum')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => [
                'required', function ($attribute, $value, $fail) {
                    if ($value !== $this->user()->verification_token) {
                        $fail('The ' . $attribute . ' is invalid');
                    }
                }
            ],

        ];
    }
}