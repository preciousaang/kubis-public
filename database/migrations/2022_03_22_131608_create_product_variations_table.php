<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductVariationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id');
            $table->string('name');
            $table->text('image');
            $table->text('slug');
            $table->string('sku', 20)->nullable();
            $table->double('measurement_value')->nullable();
            $table->string('measurement_unit')->nullable();
            $table->integer('current_stock')->default(0);
            $table->integer('stock_threshold')->default(0);
            $table->integer('reserved_stock')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variations');
    }
}