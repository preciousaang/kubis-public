<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_id' => $this->order,
            'amount' => $this->amount,
            'reference' => $this->reference,
            'status' => $this->status,
            'paid_at' => $this->paid_at,
            'currency' => $this->currency,
        ];
    }
}