<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\PaymentsController;
use App\Http\Controllers\POSController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\StocksController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::prefix('/auth')->group(function () {

    Route::middleware('auth:sanctum')->group(function () {
        Route::middleware('user-is-active')->get('/user', [UserController::class, 'user']);
        Route::post('/verify-email', [UserController::class, 'verifyEmail']);
        Route::post('/resend-verification-email', [UserController::class, 'resendVerificationEmail']);
    });

    Route::middleware('guest:sanctum')->group(function () {
        Route::post('/login', [LoginController::class, 'login']);
        Route::post('/request-password-reset', [LoginController::class, 'requestPasswordReset']);
    });
});


//Stock Routes
Route::prefix('/stocks')->group(function () {
    Route::get('/check', [StocksController::class, 'check']);
});



// Order Routes;
Route::prefix('/orders')->middleware('auth:sanctum')->group(function () {
    Route::get('/', [OrdersController::class, 'index']);
    Route::post('/', [OrdersController::class, 'create']);
    Route::get('/single/{id}', [OrdersController::class, 'single']);
});

//Products Routes
Route::prefix('/products')->group(function () {
    Route::get('/', [ProductsController::class, 'index']);
    Route::get('/single/{slug}', [ProductsController::class, 'single']);
    Route::middleware('auth:sanctum')->group(function () {
        Route::get('/single-variation/{sku}', [ProductsController::class, 'singleVariation']);
    });
});

//Category Routes

Route::prefix('/categories')->group(function () {
    Route::get('/', [CategoriesController::class, 'index']);
});



Route::prefix('pos')->group(function () {
    Route::middleware('auth:sanctum')->group(function () {
        Route::post('/', [POSController::class, 'create']);
    });
});


Route::prefix('sales')->group(function () {
    Route::middleware('auth:sanctum')->group(function () {
        Route::get('/single/{unique_id}', [SalesController::class, 'single']);
        Route::get('/store', [SalesController::class, 'myStoreSales']);
    });
});

//API Callback endpoints
Route::get('/payment/callback', [PaymentsController::class, 'verifyPayment']);