<?php

namespace App\Http\Livewire;

use App\Models\Store;
use Livewire\Component;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class AddUser extends Component
{
    use AuthorizesRequests;


    public $stores;
    public $roles;
    public $firstName,
        $lastName,
        $username,
        $email,
        $phone,
        $password,
        $password_confirmation,
        $store,
        $role,
        $is_active = false;

    protected $messages = [
        'phone.phone' => 'Invalid phone number',
        'store.prohibited_if' => 'Must be empty if role is User'
    ];

    protected function getRules()
    {
        $allowedRoles = Role::where('name', '<>', 'user')->get()->modelKeys();
        $userRole = Role::whereName('user')->first();

        return [
            'firstName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'email' => 'required|email|unique:users,email',
            'username' => 'required|unique:users,username|alpha_num',
            'password' => ['required', Password::min(8)->numbers(), 'confirmed'],
            'phone' => ['required', Rule::phone()->country(['NG']), 'unique:users,phone'],
            'is_active' => 'required|boolean',
            'role' => 'required|exists:roles,id',
            'store' => ['nullable', Rule::requiredIf(fn () => in_array($this->role, $allowedRoles)), "prohibited_if:role,{$userRole->id}", 'exists:stores,id']
        ];
    }

    public function render()
    {
        return view('livewire.add-user');
    }

    public function addUser()
    {
        $this->authorize('create', User::class);
        $this->validate();
        $user = User::create([
            'role_id' => $this->role,
            'username' => $this->username,
            'old_password' => bcrypt($this->password),
            'email' => $this->email,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'is_active' => $this->is_active,
            'store_id' => $this->store ?? null,
            'phone' => $this->phone
        ]);
        $this->resetExcept('stores', 'roles');
        session()->flash('user_added', 'User Added Successfully');
        $this->emitUp('userAdded');
    }
}