<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AdminDashboard extends Component
{
    public $sales;

    public function render()
    {
        return view('livewire.admin-dashboard');
    }

    public function fetchTodaysSales()
    {
    }

    public function fetchYesterdaySales()
    {
    }

    public function fetchTotalSales()
    {
    }

    public function mount()
    {
    }
}