<?php

namespace Kubis\Repository\Sales;



interface SalesRepositoryInterface
{

    public function fetchSales($per_page, $opt = []);
}