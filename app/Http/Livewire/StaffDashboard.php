<?php

namespace App\Http\Livewire;

use Livewire\Component;

class StaffDashboard extends Component
{
    public function render()
    {
        return view('livewire.staff-dashboard');
    }
}
