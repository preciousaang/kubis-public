<?php

return [
    'api_url' => env('FLUTTERWAVE_API_URL'),
    'secret_key' => env('FLUTTERWAVE_SECRET_KEY'),
    'public_key' => env('FLUTTERWAVE_PUBLIC_KEY'),
];