<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use HasFactory;


    public function getIsHQAttribute()
    {
        return (bool)$this->is_headquarters;
    }

    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }

    public function sales()
    {
        return $this->hasMany(Sale::class);
    }

    public function soldItems()
    {
        return $this->hasManyThrough(SaleItem::class, Sale::class);
    }

    public function stockRequests()
    {
        return $this->hasMany(StockRequest::class, 'requesting_store');
    }

    public function stockAsks()
    {
        return $this->hasMany(StockRequest::class, 'answering_store');
    }
}