<?php

namespace App\Http\Livewire\Inventory;

use App\Models\ProductVariation;
use App\Models\Stock;
use App\Models\Store;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;

class AssignStocks extends Component
{
    public $slug;
    public $variation;
    public $stocks = [];
    public $unassignedStores = [];

    protected $listeners = ['stockAdded' => 'reload'];



    public $newStock;


    protected function getRules()
    {
        return [
            'newStock.current_stock' => 'required|integer',
            'newStock.stock_threshold' => 'required|integer',
            'newStock.selling_price' => 'required|numeric',
            'newStock.cost_per_unit' => 'required|numeric',
            'newStock.store_id' => 'required|exists:stores,id'
        ];
    }

    public function mount($slug)
    {
        $this->slug = $slug;
        $this->reload();
        //TODO change this to make it appear when new Stock is selected

    }

    public function render()
    {
        return view('livewire.inventory.assign-stocks');
    }

    public function reload()
    {
        $this->variation = ProductVariation::where('slug', $this->slug)->firstOrFail();
        $this->stocks = $this->variation->stocks;
        $this->unassignedStores = Store::whereDoesntHave('stocks', function (Builder $q) {
            $q->where('product_variation_id', '=', $this->variation->id);
        })->get();
        $this->newStock = new Stock();
    }

    public function addStock()
    {
        $this->validate();
        $this->newStock->product_variation_id = $this->variation->id;
        $this->newStock->save();
        $this->reset('newStock');
        $this->emitSelf('stockAdded');
        session()->flash('stock_added', 'Stock has been added');
    }
}