@section('title', 'Manage Store')
<div>
    @livewire('nav-bar')
    <div class="container-fluid">
        <div class="row gy-2 justify-content-center">
            @can('create', App\Models\Store::class)
                <div class="col-md-4">
                    @livewire('store.add-store')
                </div>
            @endcan
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            Manage Stores
                        </h4>
                        <x-alert type="success" message="store_edited" />
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-center">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Is Headquarters</th>
                                        <th>Date Created</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($stores as $store)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $store->name }}</td>
                                            <td>{{ $store->address }}</td>
                                            <td>{!! $store->is_headquarters ? '<i class="fa fa-check-circle text-success fa-2x"></i>' : '' !!}</td>
                                            <td>{{ $store->created_at }}</td>
                                            <td>
                                                <button data-bs-toggle="modal" data-bs-target="#editStoreModal"
                                                    wire:click="selectStore({{ $store }})"
                                                    class="btn btn-sm btn-primary">Edit</button>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7">You have no shops. Create One</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-end">
                            {{ $stores->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-modal id="editStoreModal" :dismissible="false">
        <x-slot name='title'>Edit Store</x-slot>

        <form wire:submit.prevent="editStore">
            <div class="mb-3">
                <label for="storeName" class="form-label">Store Name</label>
                <input type="text" id="storeName" wire:model="selectedStore.name"
                    class="form-control @error('selectedStore.name') is-invalid @enderror">
                <x-invalid-feedback field="selectedStore.name" />
            </div>
            <div class="mb-3">
                <label for="storeName" class="form-label">Store Address</label>
                <textarea id="storeName" wire:model="selectedStore.address"
                    class="form-control @error('selectedStore.address') is-invalid @enderror"></textarea>
                <x-invalid-feedback field="selectedStore.address" />
            </div>
            <div class="mb-3">
                <div class="form-check form-switch">
                    <input class="form-check-input  @error('isHQ') is-invalid @enderror" wire:model="isHQ"
                        type="checkbox" role="switch" id="isHQ">
                    <x-invalid-feedback field="isHQ" />
                    <label class="form-check-label" for="isHQ">Is headquarters</label>
                    @unless(isset($selectedStore) && $selectedStore->is_headquarters)
                        <div class="form-text">
                            This will disable the headquarters status of the you HQ.
                        </div>
                    @endunless
                </div>
            </div>
            <div class="mb-3">
                <div class="d-grid">
                    <button type="submit" class="btn btn-secondary">Edit Store</button>
                </div>
            </div>
        </form>
    </x-modal>
    <script>
        document.addEventListener('livewire:load', () => {
            document.addEventListener('DOMContentLoaded', () => {
                const editStoreModalEl = selectId('editStoreModal')
                const editStoreModal = new Modal(editStoreModalEl);

                Livewire.on('storeEdited', () => {
                    editStoreModal.hide();
                })
            })

        })
    </script>
</div>
