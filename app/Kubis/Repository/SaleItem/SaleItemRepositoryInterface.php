<?php

namespace Kubis\Repository\SaleItem;



interface SaleItemRepositoryInterface
{
    public function fetchSaleItems($per_page, $opt = []);
}