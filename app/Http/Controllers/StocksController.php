<?php

namespace App\Http\Controllers;

use App\Http\Requests\StockCheckRequest;
use App\Models\Stock;
use App\Models\Store;
use Illuminate\Http\Request;

class StocksController extends Controller
{
    //
    public function check(StockCheckRequest $request)
    {
        $hqStore  = Store::where('is_headquarters', true)->first();
        $stock = Stock::where('store_id', $hqStore->id)
            ->where('product_variation_id', $request->variation_id)
            ->first();
        if ($stock->current_stock < $request->count) {
            // the requested count is more than the current stock
            return response()->json(['qty_available' => false, 'stock' => $stock], 200);
        } else {
            //the current stock is more than the asked count
            return response()->json(['qty_available' => true, 'stock' => $stock], 200);
        }
    }
}