<div class="col-sm-6 col-12">
    <div class="card">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="{{ asset('storage/uploads/' . $variation->image) }}"
                    style="height: 100%; width: 100%; max-height: 500px;" class="img-fluid rounded-start"
                    alt="{{ $variation->name }}">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">{{ $variation->name }}</h5>
                    <h6 class="card-subitle">SKU: {{ $variation->sku }}</h6>
                    <p class="card-text">Measurement: {{ $variation->measurement_value }}
                        {{ $variation->measurement_unit }}
                    </p>
                    <p class="card-text"><small class="text-muted">Total Store Stocks:
                            {{ $stock_count }}
                        </small>
                    </p>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-end">
                        <button wire:click="onSelect" data-bs-toggle="modal" data-bs-target="#viewVariationModal"
                            class="btn btn-primary btn-sm">View</button>
                        &nbsp;
                        @can('create', App\Models\Stock::class)
                            <button class="btn btn-success btn-sm dropdown-toggle" data-bs-toggle="dropdown"
                                id="manage-dropdown" aria-expanded="false">Actions</button>
                            <ul class="dropdown-menu" aria-labelledby="manage-dropdown">
                                <li class=>
                                    <a wire:click="onSelect" data-bs-target="#editVariationModal" data-bs-toggle="modal"
                                        href="#" class="dropdown-item">

                                        <i class="fa fa-pencil"></i> &nbsp;
                                        Edit Variation</a>
                                </li>



                                <li>
                                    <a href="{{ route('inventory-assign-stocks', $variation->slug) }}"
                                        class="dropdown-item"> <i class="fa fa-edit"></i> &nbsp;Assign Product Stock
                                    </a>
                                </li>

                            </ul>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
