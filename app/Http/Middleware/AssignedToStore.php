<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AssignedToStore
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check()) {
            $user = $request->user();
            if (($user->is_manager || $user->is_staff) && !$user->store) {
                auth()->logout();
                return redirect()->route('login')->with('login_error', 'You are not assigned to any store. Contact the system admin.');
            }
        }

        return $next($request);
    }
}