<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Role;
use App\Models\Store;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ManageUsers extends Component
{
    use AuthorizesRequests;
    use WithPagination;
    protected $paginationTheme = "bootstrap";

    public $selectedUser;
    public $stores = [];
    public $roles = [];
    public $search;

    protected $listeners = ['userAdded'];

    protected function getRules()
    {
        $allowedRoles = Role::where('name', '<>', 'user')->get()->modelKeys();
        $userRole = Role::whereName('user')->first();
        return [

            'selectedUser.first_name' => 'required|string|max:255',
            'selectedUser.last_name' => 'required|string|max:255',
            'selectedUser.email' => ['required', 'email', Rule::unique('users', 'email')->ignore($this->selectedUser->id)],
            'selectedUser.username' => ['required', 'alpha_num', Rule::unique('users', 'username')->ignore($this->selectedUser->id)],
            'selectedUser.phone' => ['required',  Rule::phone()->country(['NG']), Rule::unique('users', 'phone')->ignore($this->selectedUser->id)],
            'selectedUser.role_id' => 'required|exists:roles,id',
            'selectedUser.store_id' => ['nullable', Rule::requiredIf(fn () => in_array($this->selectedUser->role_id, $allowedRoles)), "prohibited_if:selectedUser.role_id,{$userRole->id}", 'exists:stores,id']

        ];
    }

    public function userAdded()
    {
        $this->resetPage();
    }

    public function updatedSearch()
    {
        $this->resetPage();
    }

    public function mount()
    {
        $this->authorize('viewAny', User::class);
        $this->stores = Store::orderBy('is_headquarters', 'desc')->orderBy('name', 'asc')->get();
        $this->roles = Role::where('name', '<>', 'admin')->where('name', '<>', 'superadmin')->get();
        $this->selectedUser = new User();
    }


    protected $messages = [
        'selectedUser.phone.phone' => 'Invalid phone number',
        'selectedUser.store_id.prohibited_if' => 'Must be empty if role is User'
    ];



    public function render()
    {
        //let query vary with authenticated user role
        $authUser = User::find(auth()->id());
        $superRole = Role::whereName('superadmin')->first();

        $users = User::with(['role', 'store'])
            ->where('id', '<>', auth()->id())
            ->where('role_id', '<>', $superRole->id);

        if ($authUser->is_manager) {
            $users = $users
                ->where('store_id', $authUser->store_id);
        }

        if ($this->search) {
            $users = $users
                ->where('username', 'like', '%' . $this->search . '%')
                ->orWhere('first_name', 'like', '%' . $this->search . '%')
                ->orWhere('last_name', 'like', '%' . $this->search . '%')
                ->orWhere('email', 'like', '%' . $this->search . '%');
        }
        $users = $users->orderBy('role_id', 'asc')->orderBy('username', 'asc')->paginate(20);
        return view('livewire.manage-users', ['users' => $users]);
    }


    public function toggleActive(User $user)
    {
        $this->authorize('update', $user);
        $user->is_active = !$user->is_active;
        $user->save();
        session()->flash('user_updated', $user->is_active ? 'User is now active.' : 'User is now deactivated');
    }

    public function selectUser(User $user)
    {
        $this->selectedUser = $user;
    }

    public function updateUser()
    {
        $this->authorize('update', $this->selectedUser);
        $this->validate();
        $this->selectedUser->store_id = $this->selectedUser->store_id ? $this->selectedUser->store_id : null;
        $this->selectedUser->save();
        session()->flash('user_updated', 'User updated successfully');
        $this->emit('userUpdated');
    }
}