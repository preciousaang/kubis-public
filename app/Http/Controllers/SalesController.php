<?php

namespace App\Http\Controllers;

use App\Http\Resources\SaleResource;
use App\Http\Resources\SalesCollection;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Kubis\Services\Sales\SalesServiceInterface;

class SalesController extends Controller
{
    //TODO: List sales
    //TODO: Query and perform operation on sales
    private $saleService;

    public function __construct(SalesServiceInterface $saleService)
    {
        $this->saleService = $saleService;
    }


    public function single(Request $request, $unique_id)
    {
        $sale = $this->saleService->singleSale($unique_id);
        $this->authorize('view', $sale);


        return response()->json(['sale' => new SaleResource($sale)]);
    }


    public function myStoreSales(Request $request)
    {
        $user = $request->user('sanctum');
        $perPage = $request->get('perPage') ?? 10;
        $sales = $this->saleService->storeSales($user->store_id, $perPage);
        return response()->json(
            // $sales

            new SalesCollection($sales)


        );
    }
}