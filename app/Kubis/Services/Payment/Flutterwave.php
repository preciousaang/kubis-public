<?php

namespace Kubis\Services\Payment;

use Illuminate\Support\Facades\Http;


class Flutterwave implements PaymentInterface
{
    public function __construct($apiUrl, $publicKey, $secretKey)
    {
        $this->apiUrl = $apiUrl;
        $this->publicKey = $publicKey;
        $this->secretKey = $secretKey;
    }

    private function makeRequest()
    {
        return Http::withToken($this->secretKey)->acceptJson()->baseUrl($this->apiUrl);
    }

    public function verifyTransaction(string $transaction_id)
    {
    }
}