<?php

namespace App\Http\Livewire;

use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password as FacadesPassword;
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Str;
use Livewire\Component;

class ResetPassword extends Component
{

    public $email;
    public $password;
    public $password_confirmation;

    protected function getRules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => ['required', Password::min(8)->numbers(), 'confirmed']
        ];
    }

    public function render()
    {
        return view('livewire.reset-password');
    }

    public function mount($token)
    {
        $this->token = $token;
    }

    public function resetPassword()
    {
        $this->validate();
        $status = FacadesPassword::reset(
            $this->only(['email', 'password', 'password_confirmation', 'token']),
            function ($user, $password) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));
                $user->save();
                $user->tokens()->delete();
                event(new PasswordReset($user));
            }
        );
        if ($status === FacadesPassword::PASSWORD_RESET) {
            session()->flash('reset_done', 'Password reset successful');
            $this->resetExcept('token');
            $this->resetErrorBag();
        } else {
            $this->addError('email', __($status));
        }
    }
}