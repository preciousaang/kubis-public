<div class="container-fluid mt-3">
    <div class="row gy-3">
        <div class="col-md-4">
            <div class="card shadow">
                <div class="card-header bg-purple text-center fw-bold text-uppercase text-white">
                    Today's Sales
                </div>
                <div class="card-body">
                    <h3 class="card-title text-center">
                        &#8358;{{ $todayRevenue }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow">
                <div class="card-header bg-info text-center fw-bold text-uppercase text-white">
                    Today's Profit
                </div>
                <div class="card-body">
                    <h3 class="card-title text-center">
                        &#8358;{{ $todayProfit }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow">
                <div class="card-header bg-warning fw-bold text-center text-uppercase text-white">
                    Low Stocks
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped text-center table-bordered">
                            <thead>
                                <tr>
                                    <th>SKU</th>
                                    <th>Product</th>
                                    <th>Variation</th>
                                    <th>Available</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($lowStocks))
                                    @foreach ($lowStocks as $stock)
                                        <tr>
                                            <td>{{ $stock->variation->SKU }}</td>
                                            <td>{{ $stock->variation->product->name }}</td>
                                            <td>{{ $stock->variation->name }}</td>
                                            <td>{{ $stock->current_stock }}</td>
                                            <td>
                                                <a title="Manage"
                                                    href="{{ route('inventory-manage-stock', ['variation' => $stock->variation->id]) }}"
                                                    class="rounded-circle btn btn-outline-secondary btn-sm">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="5">
                                            <a href="" class="text-decoration-none">View all low stocks</a>
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="5">
                                            You have no low stocks
                                        </td>
                                    </tr>
                                @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
