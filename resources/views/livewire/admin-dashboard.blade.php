<div class="container-fluid">
    <div class="row g-2">
        <div class="col-md-4">
            <div class="card shadow">
                <div class="card-body row g-0 align-items-center">
                    <div class="col-2">
                        <i class="fa fa-money fa-2x bg-success rounded-circle p-2 text-white"></i>
                    </div>
                    <div class="col-10">
                        <h5 class="card-title text-secondary">Today's Revenue</h5>
                        <h4 class="card-title fw-bold">&#8358;{{ number_format(5000) }}</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow">
                <div class="card-body row g-0 align-items-center">
                    <div class="col-2">
                        <i class="fa fa-money fa-2x bg-info rounded-circle p-2 text-white"></i>
                    </div>
                    <div class="col-10">
                        <h5 class="card-title text-secondary">Monthly Revenue</h5>
                        <h4 class="card-title fw-bold">&#8358;{{ number_format(5000) }}</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow">
                <div class="card-body row g-0 align-items-center">
                    <div class="col-2">
                        <i class="fa fa-money fa-2x bg-warning rounded-circle p-2 text-white"></i>
                    </div>
                    <div class="col-10">
                        <h5 class="card-title text-secondary">Total Revenue</h5>
                        <h4 class="card-title fw-bold">&#8358;{{ number_format(5000) }}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="table-responsive mt-5">
        <table class="table table-striped text-center">
            <thead>
                <tr>
                    <th>SKU</th>
                    <th>Product Name</th>
                    <th>Variation</th>
                    <th>Store</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>marriage</td>
                    <td>marriage</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
