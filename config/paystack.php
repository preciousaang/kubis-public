<?php

return [
    'api_url' => env('PAYSTACK_API_URL'),
    'secret_key' => env('PAYSTACK_SECRET_KEY'),
    'public_key' => env('PAYSTACK_PUBLIC_KEY'),
];