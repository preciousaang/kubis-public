<?php

namespace App\Exports;

use App\Models\SaleItem;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class SaleItemExport implements FromQuery, WithHeadings, ShouldAutoSize, WithMapping
{

    private $rowCount = 0;
    use Exportable;

    public function forStore($store_id)
    {
        $this->store_id = $store_id;
        return $this;
    }

    public function forYears($from = '', $to = '')
    {
        $this->from = $from;
        $this->to = $to;
        return $this;
    }

    public function query()
    {
        $query = SaleItem::query();

        if (isset($this->store_id)) {
            Log::alert($this->store_id);
            $query = $query->whereExists(function ($q) {
                $q->from('sales')
                    ->where('store_id', $this->store_id)
                    ->whereColumn('sale_items.sale_id', 'sales.id');
            });
        }
        if (isset($this->from) && isset($this->to)) {
            $query = $query->whereBetween('created_at', [date($this->from), date($this->to)]);
        }
        return $query;
    }

    public function headings(): array
    {

        return [
            '#',
            'SKU',
            'Product Name',
            'Variation',
            'Quantity',
            'Price',
            'Total',
            'Date'
        ];
    }

    public function map($sale): array
    {
        return [
            ++$this->rowCount,
            $sale->variation->sku,
            $sale->variation->product->name,
            $sale->variation->name,
            $sale->quantity,
            $sale->amount,
            $sale->total_amount,
            $sale->created_at
        ];
    }

    public function prepareRows($rows)
    {

        return $rows->transform(function ($sale) {
            $sale->total_amount = '₦' . $sale->quantity * $sale->amount;
            $sale->amount = '₦' . $sale->amount;
            $sale->created_at = $sale->created_at->format('Y-m-d H:m:s');
            return $sale;
        });
    }
}