<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    use HasFactory;
    protected $fillable = [
        'store_id',
        'product_variation_id',
        'cost_price',
        'selling_price',
        'cost_per_unit',
        'current_stock',
        'stock_threshold',
        'reserved_stock'
    ];

    public function variation()
    {
        return $this->belongsTo(ProductVariation::class, 'product_variation_id');
    }

    public function getProductAttribute()
    {
        return $this->variation->product ?? null;
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}