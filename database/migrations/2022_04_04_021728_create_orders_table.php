<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->uuid('unique_id');
            $table->foreignId('user_id');
            $table->enum('payment_type', ['pay_on_delivery', 'card'])->nullable();
            $table->text('shipping_address')->nullable();
            $table->text('notes')->nullable();
            $table->dateTime('end_date')->comment('Cancelled Or Paid')->nullable();
            $table->string('status')->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}