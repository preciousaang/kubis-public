<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Product extends Model
{
    use HasFactory, HasSlug;

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function variations()
    {
        return  $this->hasMany(ProductVariation::class);
    }

    public function getVariationCountAttribute()
    {
        return $this->variations()->count();
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    public function stocks()
    {
        return  $this->hasManyThrough(Stock::class, ProductVariation::class);
    }

    public function histories()
    {
        return $this->hasManyThrough(ProductHistory::class, ProductVariation::class);
    }
}