@section('title', 'Manage Users')
<div>
    @livewire('nav-bar')
    <div class="container-fluid">
        <div class="row gy-2 justify-content-center">
            @can('create', App\Models\User::class)
                <div class="col-md-4">
                    @livewire('add-user', ['stores'=>$stores, 'roles'=>$roles])
                </div>
            @endcan
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-center h3">
                        Manage Users
                    </div>
                    <div class="card-body">
                        <x-alert type="success" message="user_updated" />
                        <div class="d-flex justify-content-between">
                            <div></div>
                            <div class="d-flex align-items-center gx-2">
                                <input placeholder="Search Users" type="text" wire:model.debounce.500ms="search"
                                    class="form-control form-control-sm">
                                <div wire:loading wire:target="search" class="spinner-border" role="status">
                                    <span class="visually-hidden">Loading...</span>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive mt-3">
                            <table class="table table-bordered table-striped text-center rounded-top">
                                <thead>
                                    <tr>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Phone</th>
                                        <th>Role</th>
                                        <th>Store</th>
                                        <th>Active</th>

                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($users as $user)
                                        <tr>
                                            <td class="fw-bold">{{ $user->username }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->first_name }}</td>
                                            <td>{{ $user->last_name }}</td>
                                            <td>{{ $user->phone }}</td>
                                            <td>
                                                @if ($user->role)
                                                    {{ $user->role->full_name }}
                                                @endif
                                            </td>
                                            <td>
                                                @if ($user->store)
                                                    {{ $user->store->name }}
                                                @else
                                                    <i class="fa fa-ban text-danger"></i>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($user->is_active)
                                                    <i class="fa fa-check-circle text-success"></i>
                                                @else
                                                    <i class="fa fa-ban text-danger"></i>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="dropdown">
                                                    <button class="btn btn-primary btn-sm dropdown-toggle"
                                                        id="user-{{ $user->id }}-actions" aria-expanded="false"
                                                        data-bs-toggle="dropdown">Actions</button>
                                                    <ul class="dropdown-menu"
                                                        aria-labelledby="#user-{{ $user->id }}-actions">
                                                        <li><a class="dropdown-item" href="#">View History</a></li>
                                                        <li><a class="dropdown-item" role="button"
                                                                wire:click="toggleActive({{ $user }})"
                                                                href="#">{{ $user->is_active ? 'Deactivate' : 'Activate' }}</a>
                                                        </li>
                                                        <li><a data-bs-toggle="modal" data-bs-target="#editUserModal"
                                                                wire:click="selectUser({{ $user }})"
                                                                class="dropdown-item" href="#">Edit</a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </td>
                                        </tr>

                                    @empty
                                        <tr>
                                            <td colspan="7">No users here</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="d-flex justify-content-end">
                            {{ $users->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-modal id="editUserModal" size="lg" :dismissable="false">
        <x-slot name="title">
            Update User
        </x-slot>
        @if ($selectedUser != null)
            <form wire:submit.prevent="updateUser" action="">
                <div class="row">
                    <div class="col">
                        <label for="" class="form-label">First Name</label>
                        <input wire:model="selectedUser.first_name" type="text"
                            class="form-control form-control-sm @error('selectedUser.first_name') is-invalid @enderror">
                        <x-invalid-feedback field="selectedUser.first_name" />
                    </div>
                    <div class="col">
                        <label for="" class="form-label">Last Name</label>
                        <input wire:model="selectedUser.last_name" type="text"
                            class="form-control form-control-sm @error('selectedUser.last_name') is-invalid @enderror">
                        <x-invalid-feedback field="selectedUser.last_name" />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for="" class="form-label">Username</label>
                        <input wire:model="selectedUser.username" type="text"
                            class="form-control form-control-sm @error('selectedUser.username') is-invalid @enderror">
                        <x-invalid-feedback field="selectedUser.username" />
                    </div>
                    <div class="col">
                        <label for="" class="form-label">E-mail</label>
                        <input wire:model="selectedUser.email" type="text"
                            class="form-control form-control-sm @error('selectedUser.email') is-invalid @enderror">
                        <x-invalid-feedback field="selectedUser.email" />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for="" class="form-label">Store</label>
                        <select wire:model="selectedUser.store_id" name=""
                            class="form-select form-select-sm @error('selectedUser.store_id') is-invalid @enderror">
                            <option value="">Select Store</option>
                            @foreach ($stores as $store)
                                <option value="{{ $store->id }}">{{ $store->name }}</option>
                            @endforeach
                        </select>
                        <x-invalid-feedback field="selectedUser.store_id" />
                    </div>
                    <div class="col">
                        <label for="" class="form-label">User Role</label>
                        <select wire:model="selectedUser.role_id" name=""
                            class="form-select form-select-sm  @error('selectedUser.role_id') is-invalid @enderror">
                            <option value="">Select Role</option>
                            @foreach ($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->full_name }}</option>
                            @endforeach
                        </select>

                        <x-invalid-feedback field="selectedUser.role_id" />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for="" class="form-label">Phone</label>
                        <input wire:model="selectedUser.phone" type="text"
                            class="form-control form-control-sm @error('selectedUser.phone') is-invalid @enderror">
                        <x-invalid-feedback field="selectedUser.phone" />
                    </div>
                </div>
                <div class="my-2">
                    <button class="btn btn-primary" type="submit">Update User</button>
                </div>

            </form>
        @else
            <h4 class="card-title">Loading....</h4>
        @endif
    </x-modal>
    <script>
        document.addEventListener('livewire:load', () => {
            document.addEventListener('DOMContentLoaded', () => {
                const editUser = selectId('editUserModal')
                const editUserModal = new Modal(editUser);

                Livewire.on('userUpdated', () => {
                    editUserModal.hide()
                })
            })

        })
    </script>
</div>
