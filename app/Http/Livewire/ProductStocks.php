<?php

namespace App\Http\Livewire;

use App\Exports\StockExport;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Stock;
use App\Models\Store;
use Illuminate\Database\Eloquent\Builder;

class ProductStocks extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search;
    public $selected_store_id;
    public $stores;
    public $store;
    public $entries = 10;



    public function render()
    {


        $stocks = Stock::with(['variation', 'store', 'variation.product', 'variation.product.category'])->where('store_id', $this->selected_store_id)->where(function (Builder $query) {
            $query->whereHas('variation', function (Builder $query) {
                $query->where('name', 'like', "%{$this->search}%") //check for variation name
                    ->orWhereHas('product', function (Builder $query) {
                        $query->where('name', 'like', "%{$this->search}%"); // check for product name
                    })->orWhere('sku', $this->search); // check for variation SKU
            });
        })->paginate($this->entries);
        return view('livewire.product-stocks', [
            'stocks' => $stocks
        ]);
    }

    public function mount()
    {
        if (!auth()->user()->is_admin) {
            $store = auth()->user()->store;
        } else {
            $this->stores = Store::orderBy('is_headquarters', 'desc')->orderBy('name', 'asc')->get();
            $store =  Store::where('is_headquarters', true)->first();
        }

        $this->selected_store_id = $store !== null ? $store->id : null;
    }

    public function updatedSearch()
    {
        $this->resetPage();
        $this->reset('entries');
    }


    public function exportPDF()
    {
        return \Excel::download(new StockExport($this->selected_store_id), 'stock.xlsx');

        // (new StockExport($this->selected_store_id))->download('stock.pdf', \Maatwebsite\Excel\Excel::MPDF);
    }
}