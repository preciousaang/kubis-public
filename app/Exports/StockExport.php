<?php

namespace App\Exports;

use App\Models\Stock;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;

class StockExport implements ShouldAutoSize, withHeadings, WithMapping, FromQuery
{
    /**
     * @return \Illuminate\Support\Collection
     */
    // public function collection()
    // {
    //     return Stock::all();
    // }

    use Exportable;


    public function __construct(int $stockId)
    {
        $this->stockId = $stockId;
    }

    public function query()
    {
        return Stock::query()->where('store_id', $this->stockId);
    }


    public function headings(): array
    {
        return [
            'SKU',
            'Product',
            'Variation',
            'Category',
            'Available Stock',
            'Price'
        ];
    }



    public function map($stock): array
    {
        return [
            $stock->variation->sku,
            $stock->product->name,
            $stock->variation->name,
            $stock->product->category->name,
            $stock->current_stock,
            '₦' . number_format($stock->selling_price, 2),
        ];
    }
}