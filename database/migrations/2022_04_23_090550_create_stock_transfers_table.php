<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_transfers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('from')->nullable()->constrained('stores', 'id');
            $table->foreignId('to')->nullable()->constrained('stores', 'id');
            $table->foreignId('accepted_by')->nullable()->constrained('users', 'id');
            $table->foreignId('sent_by')->nullable()->constrained('users', 'id');
            $table->enum('status', ['pending', 'recieved', 'cancelled'])->default('pending');
            $table->foreignId('stock_request_id')->nullable()->constrained()->nullOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_transfers');
    }
}