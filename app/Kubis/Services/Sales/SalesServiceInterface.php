<?php


namespace Kubis\Services\Sales;

use App\Models\Sale;

interface SalesServiceInterface
{
    public function doSale($products, $paymentType, $purchaseType, $user);

    public function singleSale($unique_id): ?Sale;

    public function storeSales($storeId, $perPage = 10, $opt = []);
}