<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentVerificationRequest;
use App\Http\Resources\TransactionResource;
use App\Models\Transaction;
use Illuminate\Http\Client\HttpClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Kubis\Services\Payment\PaymentInterface;

class PaymentsController extends Controller
{
    //TODO verify transaction
    //TODO record sales made on successful transactions
    //TODO deduct stocks made on sale from the inventory
    private $paymentService;

    public function __construct(PaymentInterface $paymentService)
    {
        $this->paymentService = $paymentService;
    }

    public function verifyPayment(PaymentVerificationRequest $request)
    {

        //Verify payment and pay for order
        try {
            $response = $this->paymentService->verifyTransaction($request->get('transaction_id'));
            $data = $response['data'];

            $transaction = Transaction::updateOrcreate([
                'reference' => $data['reference']
            ], [
                'amount' => $data['amount'],
                'status' => $data['status'],
                'paid_at' => $data['paid_at'],
                'ip_address' => $data['ip_address'],
                'currency' => $data['currency']
            ]);
            return response()->json(['status' => 'successuful', 'transaction' => new TransactionResource($transaction)]);
        } catch (HttpClientException $e) {
            return response()->json(['message' => $e->response->json('message')], 400);
        }
    }
}