<?php

namespace App\Http\Livewire\Inventory;

use Livewire\Component;

class AssignStockToStore extends Component
{
    public $stock;

    protected function getRules()
    {
        return [
            'stock.current_stock' => 'required|integer',
            'stock.stock_threshold' => 'required|integer',
            'stock.selling_price' => 'required|numeric',
            'stock.cost_per_unit' => 'required|numeric'
        ];
    }
    public function render()
    {
        return view('livewire.inventory.assign-stock-to-store');
    }

    public function updateStock()
    {
        $this->validate();
        $this->stock->save();
        session()->flash('stock_updated', 'Stock updated successfully');
    }
}