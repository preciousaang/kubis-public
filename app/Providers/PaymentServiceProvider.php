<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Kubis\Services\Payment\Flutterwave;
use Kubis\Services\Payment\PaymentInterface;
use Kubis\Services\Payment\Paystack;

class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(Paystack::class, function ($app) {
            return new Paystack(
                config('paystack.api_url'),
                config('paystack.public_key'),
                config('paystack.secret_key')
            );
        });


        $this->app->singleton(Flutterwave::class, function ($app) {
            return new Flutterwave(
                config('flutterwave.api_url'),
                config('flutterwave.public_key'),
                config('flutterwave.secret_key')
            );
        });

        $this->app->bind(PaymentInterface::class, Flutterwave::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}