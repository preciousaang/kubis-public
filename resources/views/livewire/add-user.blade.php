<x-info-card>
    <x-slot name="title">Add User</x-slot>
    <x-alert type="success" message="user_added" />
    <form wire:submit.prevent="addUser" action="">
        <div class="mb-2">
            <label for="firstName" class="form-label">First Name</label>
            <input type="text" id="firstName"
                class="form-control form-control-sm @error('firstName') is-invalid @enderror" wire:model="firstName">
            <x-invalid-feedback field="firstName" />
        </div>
        <div class="mb-2">
            <label for="lastName" class="form-label">Last Name</label>
            <input type="text" id="lastName" class="form-control form-control-sm @error('lastName') is-invalid @enderror"
                wire:model="lastName">
            <x-invalid-feedback field="lastName" />
        </div>
        <div class="mb-2">
            <label for="username" class="form-label">Username</label>
            <input type="text" id="username" class="form-control form-control-sm @error('username') is-invalid @enderror"
                wire:model="username">
            <x-invalid-feedback field="username" />
        </div>
        <div class="mb-2">
            <label for="email" class="form-label">E-mail</label>
            <input type="text" id="email" class="form-control form-control-sm @error('email') is-invalid @enderror"
                wire:model="email">
            <x-invalid-feedback field="email" />
        </div>
        <div class="mb-2">
            <label for="password" class="form-label">Password</label>
            <input type="password" id="password"
                class="form-control form-control-sm @error('password') is-invalid @enderror" wire:model="password">
            <x-invalid-feedback field="password" />
        </div>
        <div class="mb-2">
            <label for="password_confirmation" class="form-label">Confirm Password</label>
            <input type="password" id="password_confirmation"
                class="form-control form-control-sm @error('password_confirmation') is-invalid @enderror"
                wire:model="password_confirmation">
            <x-invalid-feedback field="password_confirmation" />
        </div>
        <div class="mb-2">
            <label for="phone" class="form-label">Phone</label>
            <input type="text" id="phone" class="form-control form-control-sm @error('phone') is-invalid @enderror"
                wire:model="phone">
            <x-invalid-feedback field="phone" />
        </div>
        <div class="mb-2">
            <label for="store" class="form-label  @error('store') is-invalid @enderror">Store</label>
            <select wire:model="store" name="" id="store" class="form-select form-select-sm">
                <option value="">Select Store for Staff</option>
                @foreach ($stores as $store)
                    <option value="{{ $store->id }}">{{ $store->name }}</option>
                @endforeach
            </select>
            <x-invalid-feedback field="store" />
        </div>
        <div class="mb-2">
            <label for="role" class="form-label  @error('role') is-invalid @enderror">Select Role</label>
            <select wire:model="role" class="form-select form-select-sm" name="" id="role">
                <option value="">Assign a role to the user</option>
                @foreach ($roles as $role)
                    <option value="{{ $role->id }}">{{ $role->full_name }}</option>
                @endforeach
            </select>
            <x-invalid-feedback field="role" />
        </div>
        <div class="mb-2">
            <div class="form-check form-switch">
                <input wire:model="is_active" class="form-check-input  @error('is_active') is-invalid @enderror"
                    type="checkbox" role="switch" id="isActive">
                <label class="form-check-label" for="isActive">Is Active</label>
                <x-invalid-feedback field="is_active" />
            </div>
        </div>
        <div class="mb-2">
            <div class="d-grid">
                <button type="submit" class="btn btn-sm btn-secondary">
                    Add User
                </button>
            </div>
        </div>
    </form>
</x-info-card>
