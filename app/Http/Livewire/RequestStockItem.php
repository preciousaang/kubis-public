<?php

namespace App\Http\Livewire;

use App\Models\ProductVariation;
use Livewire\Component;

class RequestStockItem extends Component
{
    public ProductVariation $variation;
    public $quantity = 0;
    public $selected = false;

    protected $listeners = ['resetItems'];

    protected function getRules()
    {
        return [
            'quantity' => 'integer|min:1'
        ];
    }

    public function resetItems()
    {
        $this->resetExcept('variation');
    }

    public function render()
    {
        return view('livewire.request-stock-item');
    }

    public function add()
    {
        $this->validate();
        $this->emitUp('addVariationAndCount',  $this->variation, $this->quantity);
    }
}