<?php

namespace App\Http\Livewire\Inventory;

use Livewire\Component;

class ManageProductVariationDetail extends Component
{
    public $variation;


    public function render()
    {
        return view('livewire.inventory.manage-product-variation-detail');
    }

    public function onSelect()
    {
        $this->emitUp('onSelect', $this->variation);
    }

    public function mount($variation)
    {
        $this->variation = $variation;
        $this->stock_count = $variation->stock_count;

        if (!auth()->user()->is_admin) {
            $this->stock_count = $variation
                ->stocks()
                ->where('store_id', auth()->user()->store_id)
                ->sum('current_stock');
        }
    }
}