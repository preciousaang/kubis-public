@section('title', 'Requests to your store')
<div>
    @livewire('nav-bar')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center h5 fw-bold">
                        Requests to the store
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered text-center">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Sent By</th>
                                        <th>Items Count</th>
                                        <th>Requesting Store</th>
                                        <th>Date</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        @foreach ($stockRequests as $request)
                                            <td>{{ $request->created_at }}</td>
                                            <td>{{ $request->requesting_user->full_name }}</td>
                                            <td>{{ $request->requested_items()->count() }}</td>
                                            <td>{{ $request->requester->name }}</td>
                                            <td>{{ $request->created_at->toDateTimeString() }}</td>
                                            <td>
                                                <a href="{{ route('single-request-to-store', $request->id) }}"
                                                    class="btn btn-sm btn-info">View Request</a>
                                            </td>
                                        @endforeach
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-end">
                            {{ $stockRequests->links() }}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
