<?php

namespace App\Http\Livewire\Sales;

use App\Models\Order;
use Livewire\Component;

class ManageOrder extends Component
{
    public $order;


    public function render()
    {
        return view('livewire.sales.manage-order');
    }

    public function mount(Order $order)
    {
        $this->order = $order;
    }
}