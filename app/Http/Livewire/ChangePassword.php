<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;
use Livewire\Component;

class ChangePassword extends Component
{
    public $old_password;
    public $password;
    public $password_confirmation;

    protected function getRules()
    {
        $rules = [
            'old_password' => 'required|current_password',
            'password' => ['required', 'confirmed', Password::min(8)->numbers()],
        ];
        return $rules;
    }
    public function render()
    {
        return view('livewire.change-password');
    }

    public function changePassword()
    {
        $this->validate();
        $user = User::find(auth()->id());
        $user->forceFill([
            'password' => Hash::make($this->password),
        ]);
        $user->save();

        session()->flash('password_changed', 'Your password has been changed');
        $this->reset();
    }
}